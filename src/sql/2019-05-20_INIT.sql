-- TALLIES
DROP TABLE IF EXISTS public.tallies CASCADE;
CREATE TABLE tallies (
    id bigserial NOT NULL,
    create_timestamp timestamp without time zone,
    last_update_timestamp timestamp without time zone,
    name character varying(255),
    account_id bigint,
    access character varying(64)
);
ALTER TABLE IF EXISTS tallies ADD CONSTRAINT tallies_pkey PRIMARY KEY (id);
ALTER TABLE tallies ADD CONSTRAINT fksa7yxcokuo4g1thw8h63oaxfx FOREIGN KEY (account_id) REFERENCES users(id);
CREATE INDEX tallies_account_id_idx
    ON public.tallies USING btree
    (account_id ASC NULLS LAST)
    TABLESPACE pg_default;
CREATE INDEX tallies_access_idx
    ON public.tallies USING btree
    (access ASC NULLS LAST)
    TABLESPACE pg_default;
CREATE INDEX tallies_name_idx
    ON public.tallies USING btree
    (name COLLATE pg_catalog."default" text_pattern_ops)
    TABLESPACE pg_default;


-- CARDS
DROP TABLE IF EXISTS public.cards CASCADE;
CREATE TABLE cards (
    id bigserial NOT NULL,
    create_timestamp timestamp without time zone,
    update_timestamp timestamp without time zone,
    tally_id bigint
);
ALTER TABLE cards ADD CONSTRAINT cards_pkey PRIMARY KEY (id);
ALTER TABLE cards ADD CONSTRAINT fk5exh8bhkhkmmq12ncexf0blto FOREIGN KEY (tally_id) REFERENCES tallies(id);
CREATE INDEX cards_tally_id_idx
    ON public.cards USING btree
    (tally_id)
    TABLESPACE pg_default;
    

-- CARD_PARTS
DROP TABLE IF EXISTS public.card_parts CASCADE;
CREATE TABLE card_parts (
    part_type character varying(31) NOT NULL,
    id bigserial NOT NULL,
    content_type character varying(255),
    content text,
    create_timestamp timestamp without time zone,
    sequence_id bigint,
    card_id bigint
);
ALTER TABLE card_parts ADD CONSTRAINT card_parts_pkey PRIMARY KEY (id);
ALTER TABLE card_parts ADD CONSTRAINT fks10vif6p7f2wu5c2utwpdmito FOREIGN KEY (card_id) REFERENCES cards(id);
CREATE INDEX card_parts_part_type_idx
    ON public.card_parts USING btree
    (part_type COLLATE pg_catalog."default")
    TABLESPACE pg_default;
CREATE INDEX card_parts_sequence_idx
    ON public.card_parts USING btree
    (sequence_id)
    TABLESPACE pg_default;
CREATE INDEX card_parts_card_id_idx
    ON public.card_parts USING btree
    (card_id)
    TABLESPACE pg_default;
CREATE INDEX part_type_card_id_idx
    ON public.card_parts USING btree
    (part_type ASC NULLS LAST, card_id ASC NULLS LAST)
    TABLESPACE pg_default;


-- CARD_EVENTS
DROP TABLE IF EXISTS public.cards_events CASCADE;
CREATE TABLE cards_events (
    id bigserial NOT NULL,
    event character varying(255),
    value text,
    insert_timestamp timestamp without time zone,
    card_id bigint
);
ALTER TABLE cards_events ADD CONSTRAINT cards_events_pkey PRIMARY KEY (id);
ALTER TABLE cards_events ADD CONSTRAINT fkdd6uch9y63kbo10bg3ay179fm FOREIGN KEY (card_id) REFERENCES cards(id);

-- SUPER MEMO 2
DROP SCHEMA IF EXISTS sm2 CASCADE;
CREATE SCHEMA sm2;
--DROP TABLE IF EXISTS sm2.card_outcome CASCADE;
CREATE TABLE sm2.card_outcome
(
    id bigserial NOT NULL,
    last_revision timestamp without time zone,
    next_revision DATE,
    last_interval bigint,
    easiness_factor double precision,
    card_id bigint,
    CONSTRAINT card_outcome_pkey PRIMARY KEY (id),
    CONSTRAINT card_outcome_card_id_key UNIQUE (card_id)
,
    CONSTRAINT card_outcome_card_id_fkey FOREIGN KEY (card_id)
        REFERENCES public.cards (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
--DROP INDEX IF EXISTS sm2.card_outcome_cardid_idx;
CREATE INDEX card_outcome_cardid_idx ON sm2.card_outcome USING btree (card_id) TABLESPACE pg_default;
--DROP INDEX IF EXISTS sm2.outcome_last_rev_idx;
CREATE INDEX outcome_last_rev_idx ON sm2.card_outcome USING btree (last_revision) TABLESPACE pg_default;
--DROP INDEX IF EXISTS sm2.outcome_next_rev_idx;
CREATE INDEX outcome_next_rev_idx ON sm2.card_outcome USING btree (next_revision) TABLESPACE pg_default;

-- 21.11.2018
ALTER TABLE sm2.card_outcome add column first_revision DATE;
--DROP INDEX IF EXISTS outcome_fist_rev_idx;
CREATE INDEX outcome_fist_rev_idx ON sm2.card_outcome USING btree (first_revision) TABLESPACE pg_default;
--DROP INDEX IF EXISTS cards_outcome_first_last_rev_idx;
CREATE INDEX cards_outcome_first_last_rev_idx
    ON sm2.card_outcome USING btree
    (first_revision ASC NULLS LAST, last_revision ASC NULLS LAST)
    TABLESPACE pg_default;
    