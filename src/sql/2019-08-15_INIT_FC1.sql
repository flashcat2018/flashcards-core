CREATE SCHEMA fc1;

CREATE TABLE fc1.card_ratings
(
    id bigserial,
    first_revision date,
    last_revision date,
    next_revision date,
    "interval" integer,
    easiness_factor double precision,
    repetition integer,
    card_id bigint,
    CONSTRAINT card_ratings_pkey PRIMARY KEY (id)
);