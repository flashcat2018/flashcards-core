CREATE INDEX cards_events_card_id_idx
    ON core.cards_events USING btree
    (card_id ASC NULLS LAST)
    TABLESPACE pg_default;