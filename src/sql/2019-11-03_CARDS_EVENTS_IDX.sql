CREATE INDEX card_events_event_idx
    ON public.cards_events USING btree
    (event COLLATE pg_catalog."default")
    TABLESPACE pg_default;

CREATE INDEX card_events_insert_timestamp_idx
    ON public.cards_events USING btree
    (insert_timestamp)
    TABLESPACE pg_default;
    