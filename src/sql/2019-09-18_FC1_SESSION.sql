CREATE TABLE fc1.session
(
    id bigserial,
    last_activity timestamp without time zone,
    phase character varying(64),
    tally_id bigint,
    CONSTRAINT session_pkey PRIMARY KEY (id)
);

CREATE INDEX fc1_session_tally_idx
    ON fc1.session USING btree
    (tally_id)
    TABLESPACE pg_default;
