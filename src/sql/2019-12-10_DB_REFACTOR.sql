create schema core;

alter table fc1.card_ratings set schema core;
alter table fc1.session set schema core;
alter table public.boxes set schema core;
alter table public.card_parts set schema core;
alter table public.cards set schema core;
alter table public.cards_events set schema core;
alter table public.cards_metric set schema core;
alter table public.tallies set schema core;
alter table sm2.card_outcome set schema core;

alter table core.card_outcome rename to sm2_card_outcome;
alter table core.session rename to fc1_session;
alter table core.card_ratings rename to fc1_card_ratings;

DROP SCHEMA fc1;
DROP SCHEMA sm2;

alter table core.fc1_card_ratings add column tally_id bigint;
update core.fc1_card_ratings set tally_id=(select tally_id from core.cards where id=card_id);
CREATE INDEX fc1_card_ratings_card_id_idx
    ON core.fc1_card_ratings USING btree
    (card_id ASC NULLS LAST)
    TABLESPACE pg_default;
CREATE INDEX fc1_card_ratings_tally_id_idx
    ON core.fc1_card_ratings USING btree
    (tally_id ASC NULLS LAST)
    TABLESPACE pg_default;