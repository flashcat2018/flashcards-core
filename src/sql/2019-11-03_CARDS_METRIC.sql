-- Table: public.cards_metric

-- DROP TABLE public.cards_metric;

CREATE TABLE public.cards_metric
(
    id bigserial,
    card_id bigint,
    first_revision timestamp without time zone,
    last_revision timestamp without time zone,
    CONSTRAINT cards_metric_pkey PRIMARY KEY (id),
    CONSTRAINT cards_metric_card_id_fkey FOREIGN KEY (card_id)
        REFERENCES public.cards (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE public.cards_metric
    ADD CONSTRAINT cards_metric_card_id_key UNIQUE (card_id);

CREATE INDEX cards_metric_card_id_idx
    ON public.cards_metric USING hash
    (card_id)
    TABLESPACE pg_default;
    