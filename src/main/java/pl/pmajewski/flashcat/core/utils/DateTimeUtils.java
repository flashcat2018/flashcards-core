package pl.pmajewski.flashcat.core.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateTimeUtils {

	public static LocalDateTime convert(LocalDate localDate) {
		return localDate.atTime(0, 0, 0);
	}
}
