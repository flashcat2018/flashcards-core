package pl.pmajewski.flashcat.core.learn.engine;

import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.exception.CardNotFoundException;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.exception.LearnException;
import pl.pmajewski.flashcat.core.learn.exception.LearnExceptionWrapper;

@Component
@Transactional
@Slf4j
public class LearnEngine {

	private LearnAlgorithm algorithm;
	private CardRepository cardRepository;
	
	public LearnEngine(LearnAlgorithm learnAlgorithm, CardRepository cardRepo) {
		this.algorithm = learnAlgorithm;
		this.cardRepository = cardRepo;
	}

	public void judge(CardJudgePojo body) {
		try {
			algorithm.judge(body);
			refreshMetric(body.getCardId());
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}

	public Optional<Long> selectNextCard(Long tallyId) {
		try {
			return algorithm.selectNextCard(tallyId);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}

	public void resetTally(Long tallyId) {
		cardRepository.streamPure(tallyId)
			.forEach(i -> {
				try {
					algorithm.resetCard(i);
					refreshMetric(i);
				} catch (LearnException e) {
					log.error("["+Auth.userId()+"] Cannot reset card="+i.getId()+" in tally="+tallyId, e);
					e.printStackTrace();
				}
			});
	}

	public void resetCard(Long cardId) {
		try {
			Card card = cardRepository.findById(cardId)
				.orElseThrow(CardNotFoundException::new);
			algorithm.resetCard(card);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}

	public void initCard(Card card) {
		try {
			algorithm.initCard(card);
			refreshMetric(card);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}
	
	public void deleteCard(Long cardId) {
		try { 
			algorithm.deleteCard(cardId);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}
	
	public void deactivateCard(Long cardId) {
		try {
			algorithm.deactivateCard(cardId);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}
	
	public void activateCard(Long cardId) {
		try {
			algorithm.activateCard(cardId);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}
	
	public void move(Long cardId, Long tallyId) {
		try {
			algorithm.move(cardId, tallyId);
		} catch (LearnException e) {
			throw new LearnExceptionWrapper(e);
		}
	}
	
	private void refreshMetric(Long cardId) throws LearnException {
		Optional<Card> card = cardRepository.findById(cardId);
		refreshMetric(card.get());
	}
	
	private void refreshMetric(Card card) throws LearnException {
		card.updateMetric(algorithm.getMetric(card));
		cardRepository.save(card);
	}
}
