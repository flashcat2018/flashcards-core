package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config.FC1Properties;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;

@CommonsLog
public class FC1NoveltyPhase extends FC1SelectorPhase {

	public FC1NoveltyPhase(FC1Selector selector) {
		super(selector);
	}

	@Override
	public Optional<Long> next(Long tallyId) {
		Optional<Long> result = getNovelty(tallyId);
		log.info("["+Auth.userId()+"] Card id="+result+" selected at phase=NOVELTY");
		return result;
		
	}

	private Optional<Long> getNovelty(Long tallyId) {
		List<Long> repetableNovleties = getNoveltiesToRepeat(tallyId);
		log.info("["+Auth.userId()+"] Available repetable novelties="+Arrays.toString(repetableNovleties.toArray()));
		List<Long> novelties = getFreshNovelties(tallyId, FC1Properties.NOVELTY_BUCKET_SIZE - repetableNovleties.size());
		repetableNovleties.addAll(novelties);
		return randomizeElement(repetableNovleties);
	}
	
	private List<Long> getNoveltiesToRepeat(Long tallyId) {
		return selector.getRatingRepo().streamByTally(tallyId)
			.filter(FC1CardRating::isActive)
			.filter(i -> LocalDate.now().equals(i.getFirstRevision()))
			.filter(i -> LocalDate.now().equals(i.getNextRevision()))
			.map(i -> i.getCardId())
			.collect(Collectors.toList());
	}
	
	private List<Long> getFreshNovelties(Long tallyId, int numberOfItems) {
		List<Long> freshNovelties = selector.getRatingRepo().streamByTally(tallyId)
			.filter(FC1CardRating::isActive)
			.filter(i -> i.getFirstRevision() == null)
			.map(i -> i.getCardId())
			.collect(Collectors.toList());
		log.info("["+Auth.userId()+"] Available all freshNovelties="+Arrays.toString(freshNovelties.toArray()));
		List<Long> selectedFreshNovelties = selectNRandomItems(freshNovelties, numberOfItems);
		log.info("["+Auth.userId()+"] Selected freshNovelties="+Arrays.toString(selectedFreshNovelties.toArray()));
		return selectedFreshNovelties;
	}
	
	private <T> List<T> selectNRandomItems(List<T> items, int n) {
		List<T> result = new LinkedList<>();
		if(items.size() < n) {
			result = items;
		} else {
			Random rand = new Random();
			for(; n > 0; n--) {
				int idx = rand.nextInt(items.size());
				result.add(items.get(idx));
				items.remove(idx);
			}
		}
		return result;
	}
	
}
