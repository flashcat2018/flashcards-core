package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1AlgorithmException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session.Phase;

@CommonsLog
public class FC1RemaindPhase extends FC1SelectorPhase {

	private Predicate<FC1CardRating> existsNextRevision = i -> i.getNextRevision() != null;
	private Predicate<FC1CardRating> matureToRevision = i -> LocalDate.now().isAfter(i.getFirstRevision()) 
			&& (LocalDate.now().equals(i.getNextRevision()) || LocalDate.now().isAfter(i.getNextRevision()));
	
	public FC1RemaindPhase(FC1Selector selector) {
		super(selector);
	}

	@Override
	public Optional<Long> next(Long tallyId) throws FC1AlgorithmException {
		Optional<Long> result = Optional.empty();
		result = getRemaind(tallyId);
		
		if(result.isEmpty()) {
			result = selector.changeState(Phase.RESCUE);
		} else {
			log.info("["+Auth.userId()+"] Card id="+result+" selected at phase=REMAIND");
		}
		
		return result;
	}
	
	private Optional<Long> getRemaind(Long tallyId) {
		List<Long> ids = selector.getRatingRepo().streamByTally(tallyId)
			.filter(FC1CardRating::isActive)
			.filter(existsNextRevision)
			.filter(matureToRevision)
			.filter(i -> LocalDate.now().isAfter(i.getLastRevision()))
			.map(i -> i.getCardId())
			.collect(Collectors.toList());
		
		return randomizeElement(ids);
	}
}
