package pl.pmajewski.flashcat.core.learn.service;

import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;

public interface LearnService {
	
	CardPojo getNextCardToLearn(Long tallyId);
	
	void judgeCard(Long cardId, CardJudgePojo judge);
	
	CardPojo randomizeCard(Long tallyId);
}
