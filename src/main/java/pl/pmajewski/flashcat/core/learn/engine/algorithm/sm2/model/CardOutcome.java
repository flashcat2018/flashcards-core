package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.pmajewski.flashcat.core.card.model.Card;

@Entity
@Table(schema = "core", name = "sm2_card_outcome")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@ToString(exclude = "card")
public class CardOutcome {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "last_revision")
	private LocalDateTime lastRevision;
	
	@Column(name = "next_revision", columnDefinition = "DATE")
	private LocalDate nextRevision;
	
	@Column(name = "first_revision", columnDefinition = "DATE")
	private LocalDate firstRevision;
	
	@Column(name = "last_interval")
	private Integer lastInterval;
	
	@Column(name = "easiness_factor")
	private Double easinessFactor;
	
	@OneToOne(fetch = FetchType.LAZY)
	private Card card;
	
	public CardOutcome(Long id) {
		this.id = id;
	}
	
	public CardOutcome(Card card, Double easinessFactor, Integer lastInterval) {
		this.card = card;
		this.easinessFactor = easinessFactor;
		this.lastInterval = lastInterval;
		lastRevision = LocalDateTime.now();
		nextRevision = LocalDate.now();
		firstRevision = LocalDate.now();
	}
}
