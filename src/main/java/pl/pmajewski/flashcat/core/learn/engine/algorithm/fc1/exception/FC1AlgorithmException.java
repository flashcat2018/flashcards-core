package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception;

import pl.pmajewski.flashcat.core.learn.exception.LearnException;

public class FC1AlgorithmException extends LearnException {

	public FC1AlgorithmException() {
		super();
	}

	public FC1AlgorithmException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public FC1AlgorithmException(String message, Throwable cause) {
		super(message, cause);
	}

	public FC1AlgorithmException(String message) {
		super(message);
	}

	public FC1AlgorithmException(Throwable cause) {
		super(cause);
	}
}
