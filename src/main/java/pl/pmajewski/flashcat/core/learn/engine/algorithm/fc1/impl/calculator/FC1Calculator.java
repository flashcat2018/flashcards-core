package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.calculator;

import java.time.LocalDate;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config.FC1Properties;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;

@CommonsLog
@Component
@ConditionalOnProperty(name = "learn_algorithm", havingValue = "fc1")
public class FC1Calculator {
	
	enum FC1Answer { YES, NO };
	
	public FC1CardRating judge(FC1CardRating rating, String event) {
		log.info("["+Auth.userId()+"] Card rating before selection rating="+rating);
		FC1Answer answer = FC1Answer.valueOf(event);
		rating.incRepetition();
		
		if(rating.getFirstRevision() == null) { // nowa karta która jeszcze nigdy nie była przerabiana
			novelty(rating, answer);
		} else if(isRemainder(rating)) {
			remaind(rating, answer);
		} else if(isRescue(rating)) {
			rescue(rating, answer);
		} else if(isLearn(rating)) {
			learn(rating, answer);
		} else {
			log.error("Never should come here");
		}
		
		log.info("["+Auth.userId()+"] Card rating after selection rating="+rating);
		return rating;
	}
	
	private boolean isRemainder(FC1CardRating rating) {
		return (LocalDate.now().isAfter(rating.getNextRevision()) || LocalDate.now().equals(rating.getNextRevision()))
				&& LocalDate.now().isAfter(rating.getLastRevision());
	}
	
	private boolean isRescue(FC1CardRating rating) {
		return (LocalDate.now().isAfter(rating.getNextRevision()) || LocalDate.now().equals(rating.getNextRevision()))
				&& rating.getRepetition().equals(2);
	}
	
	private boolean isLearn(FC1CardRating rating) {
		return (LocalDate.now().isAfter(rating.getNextRevision()) || LocalDate.now().equals(rating.getNextRevision()));
	}
	
	private void learn(FC1CardRating rating, FC1Answer answer) {
		switch (answer) {
		case YES:
			learnSuccess(rating);
			break;
		case NO:
			learnFail(rating);
			break;
		}
	}
	
	private void learnSuccess(FC1CardRating rating) {
		rating.setInterval(FC1Properties.INTERVAL_INIT);
		rating.incEasinessFactor(FC1Properties.EF_SUCCESS_BONUS);
		rating.setNextRevision(rating.getInterval());
	}
	
	private void learnFail(FC1CardRating rating) {
		rating.decEasinessFactor(FC1Properties.EF_FAIL_BONUS);
	}
	
	private void rescue(FC1CardRating rating, FC1Answer answer) {
		switch (answer) {
		case YES:
			rescueSuccess(rating);
			break;
		case NO:
			rescueFail(rating);
		}
	}
	
	private void rescueSuccess(FC1CardRating rating) {
		rating.incEasinessFactor(FC1Properties.EF_SUCCESS_BONUS);
		rating.setNextRevision(rating.getInterval());
	}

	private void rescueFail(FC1CardRating rating) {
		rating.decEasinessFactor(FC1Properties.EF_FAIL_BONUS);
		rating.setInterval(FC1Properties.INTERVAL_INIT);
	}
	
	private void remaind(FC1CardRating rating, FC1Answer answer) {
		rating.setLastRevision(LocalDate.now());
		rating.initRepetition();
		
		switch (answer) {
		case YES:
			remaindSuccess(rating);
			break;
		case NO:
			remaindFail(rating);
		}
	}
	
	private void remaindSuccess(FC1CardRating rating) {
		Integer interval = rating.getInterval() * FC1Properties.INTERVAL_MULTIPLICATOR;
		rating.incEasinessFactor(FC1Properties.EF_SUCCESS_BONUS);
		rating.setInterval(interval);
		rating.setNextRevision(rating.getInterval());
	}
	
	private void remaindFail(FC1CardRating rating) {
		rating.decEasinessFactor(FC1Properties.EF_FAIL_BONUS);
		Integer interval = rating.getInterval()/2;
		rating.setInterval(interval);
	}
	
	private void novelty(FC1CardRating rating, FC1Answer answer) {
		rating.setFirstRevision(LocalDate.now());
		rating.setLastRevision(LocalDate.now());
		rating.setInterval(FC1Properties.INTERVAL_INIT);
		
		switch (answer) {
		case YES:
			noveltySuccess(rating);
			break;
		case NO:
			noveltyFail(rating);
		}
	}
	
	private void noveltySuccess(FC1CardRating rating) {
		rating.setEasinessFactor(FC1Properties.EF_INIT);
		rating.setNextRevision(rating.getInterval());
	}
	
	private void noveltyFail(FC1CardRating rating) {
		rating.setEasinessFactor(FC1Properties.EF_MIN);
		rating.setNextRevision(LocalDate.now());
	}
}
