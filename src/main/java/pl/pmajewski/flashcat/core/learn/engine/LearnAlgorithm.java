package pl.pmajewski.flashcat.core.learn.engine;

import java.util.Optional;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;
import pl.pmajewski.flashcat.core.learn.exception.LearnException;

public abstract class LearnAlgorithm {

	public abstract void judge(CardJudgePojo body) throws LearnException;
	
	/**
	 * @param tallyId
	 * @return Optional.empty() - When there are no more learning cards
	 * @throws LearnException
	 */
	public abstract Optional<Long> selectNextCard(Long tallyId) throws LearnException;
	
	public abstract void resetCard(Card card) throws LearnException;
	
	public abstract void initCard(Card card) throws LearnException;
	
	public abstract CardMetric getMetric(Card card) throws LearnException;
	
	public abstract void deleteCard(Long cardId) throws LearnException;
	
	public abstract void deactivateCard(Long cardId) throws LearnException;

	public abstract void activateCard(Long cardId) throws LearnException;
	
	public abstract void move(Long cardId, Long tallyId) throws LearnException;
}
