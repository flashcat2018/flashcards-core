package pl.pmajewski.flashcat.core.learn.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.engine.LearnEngine;
import pl.pmajewski.flashcat.core.learn.model.CardEvent;
import pl.pmajewski.flashcat.core.learn.model.CardEvent.Event;
import pl.pmajewski.flashcat.core.learn.repository.CardEventRepository;
import pl.pmajewski.flashcat.core.learn.service.LearnService;

@Service
@Transactional
public class LearnServiceImpl implements LearnService {

	private final Random rand = new Random();
	
	private LearnEngine algorithm;
	private CardRepository cardRepository;
	private CardEventRepository eventRepository;
	
	public LearnServiceImpl(LearnEngine algorithm, CardRepository cardRepository, CardEventRepository eventRepo) {
		this.algorithm = algorithm;
		this.cardRepository = cardRepository;
		this.eventRepository = eventRepo;
	}

	@Override
	public CardPojo getNextCardToLearn(Long tallyId) {
		Optional<Long> cardId = algorithm.selectNextCard(tallyId);
		CardPojo result = null;
		if(cardId.isPresent()) {
			result = cardRepository.findById(cardId.get()).get().getPojo();
		}
		return result;
	}
	
	@Override
	public void judgeCard(Long cardId, CardJudgePojo body) {
		eventRepository.save(new CardEvent(new Card(cardId), Event.LEARN, body.getEvent()));
		body.setCardId(cardId);
		algorithm.judge(body);
	}
	
	@Override
	public CardPojo randomizeCard(Long tallyId) {
		List<Card> cards = cardRepository.findByTallyId(tallyId);
		Card card = cards.get(rand.nextInt(cards.size()));
		eventRepository.save(new CardEvent(card, Event.QUIZ));
		return card.getPojo();
	}
}
