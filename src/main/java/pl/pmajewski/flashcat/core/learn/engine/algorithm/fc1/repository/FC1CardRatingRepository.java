package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;

public interface FC1CardRatingRepository extends CrudRepository<FC1CardRating, Long> {

	/**
	 * Fetch with card - performance issue
	 */
	@Query("select cr from FC1CardRating cr where cr.cardId = :cardId")
	Optional<FC1CardRating> findByCardId(@Param("cardId") Long cardId);
	
	/**
	 * Fetch with card - performance issue
	 */
	@Query("select cr from FC1CardRating cr where cr.tallyId = :tallyId")
	Stream<FC1CardRating> streamByTally(@Param("tallyId") Long tallyId);
	
	
	@Query("select count(cr) from FC1CardRating cr where cr.tallyId = :tallyId and cr.firstRevision is null")
	Long countNovelties(@Param("tallyId") Long tallyId);
	
	
}
