package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.exception;

import pl.pmajewski.flashcat.core.learn.exception.LearnException;

public class OperationNotSupported extends LearnException {

	public OperationNotSupported(String operationName) {
		super(operationName+" is not supported");
	}
}
