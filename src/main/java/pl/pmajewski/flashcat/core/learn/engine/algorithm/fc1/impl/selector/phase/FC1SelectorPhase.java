package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1AlgorithmException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;

public abstract class FC1SelectorPhase {

	protected FC1Selector selector;
	
	public FC1SelectorPhase(FC1Selector selector) {
		this.selector = selector;
	}
	
	public abstract Optional<Long> next(Long tallyId) throws FC1AlgorithmException;
	
	protected <T> Optional<T> randomizeElement(List<T> ids) {
		if(ids.isEmpty()) {
			return Optional.empty();
		} 
		
		Random rand = new Random();
		int id = rand.nextInt(ids.size());
		return Optional.ofNullable(ids.get(id));
	}
}
