package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1InvalidPhase;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session;

public class FC1PhaseFactory {
	
	private FC1Selector selector;
	
	public FC1PhaseFactory(FC1Selector selector) {
		this.selector = selector;
	}
	
	public FC1SelectorPhase get(FC1Session.Phase phase) throws FC1InvalidPhase {
		
		switch (phase) {
		case REMAIND:
			return new FC1RemaindPhase(selector);
		case RESCUE:
			return new FC1RescuePhase(selector);
		case LEARN:
			return new FC1LearnPhase(selector);
		case NOVELTY:
			return new FC1NoveltyPhase(selector);
		}
		
		throw new FC1InvalidPhase();
	}
}
