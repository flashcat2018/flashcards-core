package pl.pmajewski.flashcat.core.learn.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.learn.service.LearnService;
import pl.pmajewski.flashcat.core.learn.service.impl.LearnServiceAccessProxy;

@RestController
@CommonsLog
public class LearnController {

	private LearnService learningService;

	public LearnController(LearnServiceAccessProxy learningService) {
		this.learningService = learningService;
	}
	
	@GetMapping("/tallies/{id}/learn")
	public CardPojo getNextToLearn(@PathVariable(name = "id") Long tallyId) {
		log.info("["+Auth.userId()+"] Take card to learn by tally="+tallyId);
		return learningService.getNextCardToLearn(tallyId);
	}
	
	
	@PostMapping("/cards/{id}/learn")
	public void judegeCard(@PathVariable(name = "id") Long cardId, @RequestBody CardJudgePojo body) {
		log.info("["+Auth.userId()+"] Judge card = "+cardId+" body = "+body);
		learningService.judgeCard(cardId, body);
	}
	
	@GetMapping("/tallies/{id}/random")
	public CardPojo randomizeCard(@PathVariable Long id) {
		log.info("["+Auth.userId()+"] Randomize card from tally = "+id);
		return learningService.randomizeCard(id);
	}
}
