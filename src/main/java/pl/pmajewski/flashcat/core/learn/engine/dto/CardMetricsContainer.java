package pl.pmajewski.flashcat.core.learn.engine.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CardMetricsContainer {

	private Map<Long, CardMetric> metrics = new HashMap<>();
	
	public CardMetricsContainer(List<CardMetric> metrics) {
		for (CardMetric metric : metrics) {
			this.metrics.put(metric.cardId, metric);
		}
	}
	
	public CardMetric get(Long id) {
		return metrics.get(id);
	}
}
