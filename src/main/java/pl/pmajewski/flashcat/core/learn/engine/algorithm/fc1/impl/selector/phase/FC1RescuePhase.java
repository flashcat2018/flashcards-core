package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1AlgorithmException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session.Phase;

@CommonsLog
public class FC1RescuePhase extends FC1SelectorPhase {

	private Predicate<FC1CardRating> existsLastRevision = i -> i.getLastRevision() != null;
	private Predicate<FC1CardRating> matureToRevision = i -> LocalDate.now().isAfter(i.getFirstRevision()) 
			&& (LocalDate.now().equals(i.getNextRevision()) || LocalDate.now().isAfter(i.getNextRevision()));
	private Predicate<FC1CardRating> reviewedToday = i -> LocalDate.now().equals(i.getLastRevision());
	
	public FC1RescuePhase(FC1Selector selector) {
		super(selector);
	}

	@Override
	public Optional<Long> next(Long tallyId) throws FC1AlgorithmException {
		Optional<Long> result = getRescue(tallyId);
		
		if(result.isEmpty()) {
			result = selector.changeState(Phase.LEARN);
		} else {
			log.info("["+Auth.userId()+"] Card id="+result+" selected at phase=RESCUE");
		}
		
		return result;
	}

	private Optional<Long> getRescue(Long tallyId) {
		List<Long> ids = selector.getRatingRepo().streamByTally(tallyId)
			.filter(FC1CardRating::isActive)
			.filter(existsLastRevision)
			.filter(matureToRevision)
			.filter(reviewedToday)
			.filter(i -> i.getRepetition().equals(1))
			.map(i -> i.getCardId())
			.collect(Collectors.toList());
		
		return randomizeElement(ids);
	}
}
