package pl.pmajewski.flashcat.core.learn.repository;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.pmajewski.flashcat.core.learn.model.CardEvent;

public interface CardEventRepository extends CrudRepository<CardEvent, Long> {

	/**
	 * Fetch with card related to event - performance issue
	 * 	
	 * @param from - inclusively
	 * @param to - exclusively
	 */
	@Query("select ce from CardEvent ce join fetch ce.card c join fetch c.cardMetric where ce.card.tally.id=:tally and ce.insertTimestamp >= :from and ce.insertTimestamp < :to order by ce.insertTimestamp desc")
	Stream<CardEvent> listByTally(@Param("tally") Long tallyId, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);
	
	@Query("select ce from CardEvent ce join fetch ce.card c join fetch c.cardMetric where ce.card.tally.accountId=:accountId and ce.insertTimestamp >= :from and ce.insertTimestamp < :to order by ce.insertTimestamp desc")
	Stream<CardEvent> listByAccount(@Param("accountId") Long accountId, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);
	
}
