package pl.pmajewski.flashcat.core.learn.service.impl;

import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.auth.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.service.LearnService;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;

@Component
public class LearnServiceAccessProxy implements LearnService {

	private Auth auth;
	private TallyRepository tallyRepository;
	private CardRepository cardRepository;
	private LearnService learnService;
	
	
	public LearnServiceAccessProxy(Auth auth, TallyRepository tallyRepository, CardRepository cardRepository,
			LearnServiceImpl learnService) {
		this.auth = auth;
		this.tallyRepository = tallyRepository;
		this.cardRepository = cardRepository;
		this.learnService = learnService;
	}

	@Override
	public CardPojo getNextCardToLearn(Long tallyId) {
		tallyRepository.findById(tallyId).ifPresent(tally -> {
			if(!auth.getUserId().equals(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return learnService.getNextCardToLearn(tallyId);
	}

	@Override
	public void judgeCard(Long cardId, CardJudgePojo judge) {
		cardRepository.findById(cardId).ifPresent(card -> {
			if(!auth.getUserId().equals(card.getTally().getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		learnService.judgeCard(cardId, judge);
	}

	@Override
	public CardPojo randomizeCard(Long tallyId) {
		tallyRepository.findById(tallyId).ifPresent(tally -> {
			if(!auth.getUserId().equals(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return learnService.randomizeCard(tallyId);
	}
}
