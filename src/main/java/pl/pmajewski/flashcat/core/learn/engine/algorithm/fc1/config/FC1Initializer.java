package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config;

import java.util.concurrent.CompletableFuture;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.engine.LearnEngine;
import pl.pmajewski.flashcat.core.learn.exception.LearnExceptionWrapper;

@ConditionalOnProperty(name = "learn_algorithm", havingValue = "fc1")
@Transactional
@Slf4j
public class FC1Initializer {

	private CardRepository cardRepo;
	private LearnEngine engine;

	public FC1Initializer(CardRepository cardRepo, LearnEngine engine) {
		this.cardRepo = cardRepo;
		this.engine = engine;
	}

	@PostConstruct
	public void init() {
		CompletableFuture.runAsync(this::initialize);
	}
	
	public void initialize() {
		cardRepo.findAll()
			.forEach(i -> {
				try {
					log.debug("[FC1Initializer] Card "+i.getId()+" initialized");
					engine.initCard(i);
				} catch (LearnExceptionWrapper e) {
					log.debug("[FC1Initializer] Card "+i.getId()+" already initialized");
				} catch (Exception e) {
					log.error(e.getMessage(), e);;
				}
			});
	}
}
