package pl.pmajewski.flashcat.core.learn.service;

import java.time.LocalDate;
import java.util.Map;

public interface LearnHistoryMetrics {

	Map<LocalDate, Long> accountRepetitions(Long accountId, LocalDate from, LocalDate to);
	
	Map<LocalDate, Long> accountNovelties(Long accountId, LocalDate from, LocalDate to);
	
	Map<LocalDate, Long> learnTime(Long accountId, LocalDate from, LocalDate to);
}
