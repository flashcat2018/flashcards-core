package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.repository;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.model.CardOutcome;

public interface SM2CardOutcomeRepository extends CrudRepository<CardOutcome, Long> {
	
	@Query("select co from CardOutcome co where co.card.id = :cardId")
	CardOutcome getByCardId(@Param("cardId") Long cardId);
	
	@Query("delete from CardOutcome co where co.card.tally.id = :tallyId")
	void removeByTallyId(@Param("tallyId") Long tallyId);
	
	@Query("select co from CardOutcome co left join fetch co.card c where co.card.tally.id = :tallyId")
	Stream<CardOutcome> streamByTallyId(@Param("tallyId") Long tallyId);
}
