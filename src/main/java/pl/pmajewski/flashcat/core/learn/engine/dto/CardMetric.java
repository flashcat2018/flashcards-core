package pl.pmajewski.flashcat.core.learn.engine.dto;

import java.time.LocalDate;

import lombok.ToString;

@ToString
public class CardMetric {

	public Long cardId;
	public LocalDate firstRevision;
	public LocalDate lastRevision;
	public LocalDate nextRevision;
	
}
