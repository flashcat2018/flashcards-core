package pl.pmajewski.flashcat.core.learn.exception;

public class LearnException extends Exception {

	public LearnException() {
		super();
	}

	public LearnException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public LearnException(String message, Throwable cause) {
		super(message, cause);
	}

	public LearnException(String message) {
		super(message);
	}

	public LearnException(Throwable cause) {
		super(cause);
	}
}
