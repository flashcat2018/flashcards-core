package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.exception;

import pl.pmajewski.flashcat.core.learn.exception.LearnException;

public class SM2InvalidDate extends LearnException {

	public SM2InvalidDate() {
		super("Invalid date. This algorithm supoorts only current day estimation,");
	}
}
