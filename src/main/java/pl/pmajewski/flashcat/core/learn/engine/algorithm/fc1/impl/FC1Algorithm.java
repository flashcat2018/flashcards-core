package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.learn.engine.LearnAlgorithm;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.CardAlreadyInitializedException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.CardNotInitializedExcaption;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.calculator.FC1Calculator;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository.FC1CardRatingRepository;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository.FC1SessionRepository;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;
import pl.pmajewski.flashcat.core.learn.exception.LearnException;

@Component
@Transactional
@ConditionalOnProperty(name =  "learn_algorithm", havingValue = "fc1")
public class FC1Algorithm extends LearnAlgorithm {

	private FC1CardRatingRepository fc1RatingRepo;
	private FC1SessionRepository sessionRepository;
	private FC1Calculator calculator;
	
	public FC1Algorithm(FC1CardRatingRepository fc1RatingRepo, FC1SessionRepository sessionRepository, FC1Calculator calculator) {
		this.fc1RatingRepo = fc1RatingRepo;
		this.sessionRepository = sessionRepository;
		this.calculator = calculator;
	}
	
	@Override
	public void judge(CardJudgePojo body) throws LearnException {
		FC1CardRating rating = fc1RatingRepo.findByCardId(body.getCardId())
			.orElseThrow(CardNotInitializedExcaption::new);
		
		calculator.judge(rating, body.getEvent());
		fc1RatingRepo.save(rating);
	}

	@Override
	public Optional<Long> selectNextCard(Long tallyId) throws LearnException {
		FC1Selector selector = new FC1Selector(fc1RatingRepo, sessionRepository);
		return selector.selectNextCard(tallyId);
	}

	@Override
	public void resetCard(Card card) throws LearnException {
		FC1CardRating rating = fc1RatingRepo.findByCardId(card.getId())
			.orElseThrow(CardNotInitializedExcaption::new);
		
		fc1RatingRepo.delete(rating);
		initCard(card);
	}

	@Override
	public void initCard(Card card) throws LearnException {
		Optional<FC1CardRating> rating = fc1RatingRepo.findByCardId(card.getId());
		if(rating.isPresent()) {
			throw new CardAlreadyInitializedException();
		}
		fc1RatingRepo.save(new FC1CardRating(card));
	}

	@Override
	public CardMetric getMetric(Card card) throws LearnException {
		FC1CardRating rating = fc1RatingRepo.findByCardId(card.getId())
				.orElseThrow(CardNotInitializedExcaption::new);
		
		return rating.getMetric();
	}

	@Override
	public void deleteCard(Long cardId) throws LearnException {
		FC1CardRating rating = fc1RatingRepo.findByCardId(cardId)
			.orElseThrow(CardNotInitializedExcaption::new);
		
		fc1RatingRepo.delete(rating);
	}

	@Override
	public void deactivateCard(Long cardId) throws LearnException {
		changeCardState(cardId, false);
	}

	@Override
	public void activateCard(Long cardId) throws LearnException {
		changeCardState(cardId, true);
	}

	private void changeCardState(Long cardId, boolean state) throws LearnException {
		FC1CardRating rating = fc1RatingRepo.findByCardId(cardId)
				.orElseThrow(CardNotInitializedExcaption::new);
		
		rating.setActive(state);
		fc1RatingRepo.save(rating);
	}

	@Override
	public void move(Long cardId, Long tallyId) throws LearnException {
		FC1CardRating rating = fc1RatingRepo.findByCardId(cardId)
				.orElseThrow(CardNotInitializedExcaption::new);
		
		rating.setTallyId(tallyId);
		fc1RatingRepo.save(rating);
	}
}
