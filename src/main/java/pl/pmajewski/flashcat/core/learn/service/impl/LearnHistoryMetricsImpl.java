package pl.pmajewski.flashcat.core.learn.service.impl;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.model.Metric;
import pl.pmajewski.flashcat.core.learn.model.CardEvent;
import pl.pmajewski.flashcat.core.learn.model.CardEvent.Event;
import pl.pmajewski.flashcat.core.learn.repository.CardEventRepository;
import pl.pmajewski.flashcat.core.learn.service.LearnHistoryMetrics;
import pl.pmajewski.flashcat.core.utils.DateTimeUtils;

@Service
@Transactional
public class LearnHistoryMetricsImpl implements LearnHistoryMetrics {

	private static final long LEARN_TIME_GAP = 60l * 1000l; // 60s
	 
	private CardEventRepository cardEventRepo;
	
	public LearnHistoryMetricsImpl(CardEventRepository cardEventRepo) {
		this.cardEventRepo = cardEventRepo;
	}

	@Override
	public Map<LocalDate, Long> accountRepetitions(Long accountId, LocalDate from, LocalDate to) {
		return cardEventRepo.listByAccount(accountId, DateTimeUtils.convert(from), DateTimeUtils.convert(to).plusDays(1))
				.filter(i -> i.getEvent().equals(Event.LEARN))
				.filter(i -> i.getCard().getMetric().getFirstRevision() != null)
				.collect(Collectors.groupingBy(i -> i.getInsertTimestamp().toLocalDate()))
				.entrySet().stream()
				.collect(Collectors.toMap(i -> i.getKey(), i ->  i.getValue().stream()
						.filter(j -> !isFirstRepetition(j.getCard().getMetric(), i.getKey()))
						.map(j -> j.getCard().getId())
						.distinct()
						.count()));
	}

	@Override
	public Map<LocalDate, Long> accountNovelties(Long accountId, LocalDate from, LocalDate to) {
		return cardEventRepo.listByAccount(accountId, DateTimeUtils.convert(from), DateTimeUtils.convert(to).plusDays(1))
				.filter(i -> i.getEvent().equals(Event.LEARN))
				.filter(i -> i.getCard().getMetric().getFirstRevision() != null)
				.collect(Collectors.groupingBy(i -> i.getInsertTimestamp().toLocalDate()))
				.entrySet().stream()
				.collect(Collectors.toMap(i -> i.getKey(), i ->  i.getValue().stream()
					.filter(j -> isFirstRepetition(j.getCard().getMetric(), i.getKey()))
					.map(j -> j.getCard().getId())
					.distinct()
					.count()));
	}
	
	private boolean isFirstRepetition(Metric metric, LocalDate date) {
		if(metric.getFirstRevision() == null) {
			return false;
		}
		return metric.getFirstRevision().equals(date);
	}

	@Override
	public Map<LocalDate, Long> learnTime(Long accountId, LocalDate from, LocalDate to) {
		return cardEventRepo.listByAccount(accountId, from.atStartOfDay(), to.plusDays(1).atStartOfDay())
			.filter(i -> Event.LEARN.equals(i.getEvent()) || Event.QUIZ.equals(i.getEvent()))
			.collect(Collectors.groupingBy(i -> i.getInsertTimestamp().toLocalDate()))
			.entrySet().stream()
			.map(i -> Pair.create(i.getKey(), evaluateLearnTime(i.getValue())))
			.collect(Collectors.toMap(i -> i.getKey(), i -> i.getValue()));
	}

	/*
	 * @return learn time in sec
	 */
	private Long evaluateLearnTime(List<CardEvent> events) {
		events.sort(Comparator.comparing(CardEvent::getInsertTimestamp));
		
		long lastEvent = events.size() > 0 ? events.get(0).getInsertTimestamp().toInstant(ZoneOffset.UTC).toEpochMilli() : 0l;
		long counter = 0l;
		for (CardEvent cardEvent : events) {
			long timestamp = cardEvent.getInsertTimestamp().toInstant(ZoneOffset.UTC).toEpochMilli();
			counter += timeDifference(timestamp, lastEvent);
			lastEvent = timestamp;
		}
		
		return counter / 1000; // convertion to sec
	}
	
	private long timeDifference(long accualEvent, long previousEvent) {
		long timeDifference = accualEvent - previousEvent;
		if(timeDifference > LEARN_TIME_GAP) {
			return 0;
		} else {
			return timeDifference;
		}
	}
}
