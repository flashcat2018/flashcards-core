package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.pmajewski.flashcat.core.tally.model.Tally;

@Entity
@Table(name =  "fc1_session", schema =  "core")
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@ToString(exclude = "tally")
public class FC1Session {

	public enum Phase {
		REMAIND,
		RESCUE,
		LEARN,
		NOVELTY
	}
	
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long id;

	@Column(name = "last_activity", nullable = false)
	private LocalDate lastActivity;
	
	@Column(name =  "phase", length = 64, nullable = false)
	@Enumerated(EnumType.STRING)
	private Phase phase;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tally_id", nullable = false)
	private Tally tally;
	
	public FC1Session(Tally tally) {
		this.phase = Phase.REMAIND;
		this.tally = tally;
		this.lastActivity = LocalDate.now();
	}
	
	public void reset() {
		this.phase = Phase.REMAIND;
		this.lastActivity = LocalDate.now();
	}
}
