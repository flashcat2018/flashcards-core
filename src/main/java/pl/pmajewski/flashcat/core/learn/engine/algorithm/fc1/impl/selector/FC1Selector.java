package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector;

import java.time.LocalDate;
import java.util.Optional;

import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1AlgorithmException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase.FC1PhaseFactory;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase.FC1SelectorPhase;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session.Phase;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository.FC1CardRatingRepository;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository.FC1SessionRepository;
import pl.pmajewski.flashcat.core.tally.model.Tally;

@CommonsLog
public class FC1Selector {

	@Getter
	private FC1CardRatingRepository ratingRepo;
	private FC1SessionRepository sessionRepo;
	private FC1Session session;
	private Long tallyId;

	public FC1Selector(FC1CardRatingRepository ratingRepo, FC1SessionRepository sessionRepo) {
		this.ratingRepo = ratingRepo;
		this.sessionRepo = sessionRepo;
	}

	public Optional<Long> selectNextCard(Long tallyId) throws FC1AlgorithmException {
		this.tallyId = tallyId;
		this.session = getSession(tallyId);
		FC1PhaseFactory factory = new FC1PhaseFactory(this);
		FC1SelectorPhase phase = factory.get(session.getPhase());
		return phase.next(tallyId);
	}
	
	private FC1Session getSession(Long tallyId) {
		FC1Session session = this.sessionRepo.findByTally(new Tally(tallyId))
			.orElse(new FC1Session(new Tally(tallyId)));
	
		if(!LocalDate.now().equals(session.getLastActivity())) {
			log.info("["+Auth.userId()+"] Reset FC1 session="+session);
			session.reset();
			sessionRepo.save(session);
		}
		
		log.info("["+Auth.userId()+"] Get FC1 session="+session);
		return session;
	}
	
	public Optional<Long> changeState(Phase phase) throws FC1AlgorithmException {
		log.info("["+Auth.userId()+"] Switch FC1Selector to phase="+phase);
		session.setPhase(phase);
		sessionRepo.save(session);
		return selectNextCard(this.tallyId);
	}
}
