package pl.pmajewski.flashcat.core.learn.exception;

public class LearnExceptionWrapper extends RuntimeException {

	public LearnExceptionWrapper() {
		super();
	}

	public LearnExceptionWrapper(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public LearnExceptionWrapper(String message, Throwable cause) {
		super(message, cause);
	}

	public LearnExceptionWrapper(String message) {
		super(message);
	}

	public LearnExceptionWrapper(Throwable cause) {
		super(cause);
	}
}
