package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.impl;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.exception.SM2InvalidDate;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.model.CardOutcome;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.repository.SM2CardOutcomeRepository;
import pl.pmajewski.flashcat.core.learn.exception.LearnException;

@Slf4j
public class SM2Selector {

	private static Predicate<CardOutcome> toLearnToday = co -> 
			co.getNextRevision().isBefore(LocalDate.now())
			|| co.getNextRevision().equals(LocalDate.now());

	private SM2CardOutcomeRepository outcomeRepository;
	private CardRepository cardRepository;
	
	public SM2Selector(SM2CardOutcomeRepository outcomeRepository, CardRepository cardRepository) {
		this.outcomeRepository = outcomeRepository;
		this.cardRepository = cardRepository;
	}

	public Optional<Long> nextCard(Long tallyId) throws LearnException {
		Card card = outcomeRepository.streamByTallyId(tallyId)
			.filter(co -> co.getFirstRevision().isBefore(LocalDate.now()))
			.filter(co -> co.getNextRevision().isBefore(LocalDate.now()) || co.getNextRevision().equals(LocalDate.now()))
			.sorted(Comparator.comparing(CardOutcome::getLastRevision))
			.map(CardOutcome::getCard)
			.findFirst()
			.orElseGet(() -> new CardOutcome(-1l).getCard());
		
		if(card == null) {
			List<Card> newCards = cardRepository.streamPure(tallyId)
				.filter(c -> outcomeRepository.getByCardId(c.getId()) == null) // TODO refactor -> to many db requests
				.collect(Collectors.toList());
			card = selectNovelty(newCards);
		}
		
		return card == null ? Optional.empty() : Optional.of(card.getId());
	}

	private Card selectNovelty(List<Card> cards) {
		Card result = null;
		if(cards.size() > 0) {
			Random rand = new Random();
			int idx = rand.nextInt(cards.size());
			result = cards.get(idx);
		}
		
		return result;
	}
	
	public Long countCardsToLearn(LocalDate date, Long tallyId) throws LearnException {
		if(!LocalDate.now().equals(date)) {
			throw new SM2InvalidDate();
		}
		return outcomeRepository.streamByTallyId(tallyId)
		.filter(toLearnToday)
		.count();
	}
}
