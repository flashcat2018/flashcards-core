package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.phase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1AlgorithmException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector.FC1Selector;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session.Phase;

@CommonsLog
public class FC1LearnPhase extends FC1SelectorPhase {

	private Predicate<FC1CardRating> existsLastRevision = i -> i.getLastRevision() != null;
	private Predicate<FC1CardRating> matureToRevision = i -> LocalDate.now().isAfter(i.getFirstRevision()) 
			&& (LocalDate.now().equals(i.getNextRevision()) || LocalDate.now().isAfter(i.getNextRevision()));
	private Predicate<FC1CardRating> reviewedToday = i -> LocalDate.now().equals(i.getLastRevision());
	
	public FC1LearnPhase(FC1Selector selector) {
		super(selector);
	}

	@Override
	public Optional<Long> next(Long tallyId) throws FC1AlgorithmException {
		Optional<Long> result = getLearn(tallyId);
		
		if(result.isEmpty()) {
			result = selector.changeState(Phase.NOVELTY);
		} else {
			log.info("["+Auth.userId()+"] Card id="+result+" selected at phase=LEARN");
		}
		
		return result;
	}

	private Optional<Long> getLearn(Long tallyId) {
		Map<Integer, List<FC1CardRating>> groups = selector.getRatingRepo().streamByTally(tallyId)
			.filter(FC1CardRating::isActive)
			.filter(existsLastRevision)
			.filter(matureToRevision)
			.filter(reviewedToday)
			.collect(Collectors.groupingBy(FC1CardRating::getRepetition));
		
		Optional<Integer> min = groups.keySet().stream()
			.min(Integer::compare);
		
		List<Long> ids = groups.getOrDefault(min.orElseGet(() -> Integer.valueOf(-1)), new ArrayList<>())
			.stream()
			.map(i -> i.getCardId())
			.collect(Collectors.toList());
		
		return randomizeElement(ids);
	}
}
