package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.model.CardOutcome;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.repository.SM2CardOutcomeRepository;

public class SM2Calculator {
	
	private static final Double INITIAL_EASINESS_FACTOR = 2d;
	private static final Integer INITAL_INTERVAL = 1;

	private SM2CardOutcomeRepository cardOutcomeRepo;
	
	public SM2Calculator(SM2CardOutcomeRepository cardOutcomeRepo) {
		this.cardOutcomeRepo = cardOutcomeRepo;
	}

	public void judge(CardJudgePojo body) {
		CardOutcome outcome = cardOutcomeRepo.getByCardId(body.getCardId());
		if(outcome == null) {
			outcome = new CardOutcome(new Card(body.getCardId()), INITIAL_EASINESS_FACTOR, INITAL_INTERVAL);
		}
		
		if("YES".equals(body.getEvent())) {
			outcome = yesAnswer(outcome);
		} else {
			outcome = noAnswer(outcome);
		}
		
		cardOutcomeRepo.save(outcome);
	}
	
	private CardOutcome yesAnswer(CardOutcome outcome) {
		LocalDate nextRevision = LocalDate.now();

		if(nextRevision.isBefore(outcome.getLastRevision().toLocalDate()) || nextRevision.isEqual(outcome.getLastRevision().toLocalDate())) {
			// first box
			nextRevision = nextRevision.plusDays(1);
			outcome.setNextRevision(LocalDate.now().plusDays(1));
		} else {
			// each other box
			Double interval = outcome.getLastInterval() * outcome.getEasinessFactor();
			nextRevision = nextRevision.plusDays(interval.longValue());
			outcome.setLastRevision(LocalDateTime.now());
			outcome.setNextRevision(nextRevision);
			outcome.setLastInterval(interval.intValue());			
		}
		
		return outcome;
	}
	
	private CardOutcome noAnswer(CardOutcome outcome) {
		outcome.setLastRevision(LocalDateTime.now());
		outcome.setLastInterval(INITAL_INTERVAL);
		outcome.setEasinessFactor(INITIAL_EASINESS_FACTOR);
		return outcome;
	}
}
