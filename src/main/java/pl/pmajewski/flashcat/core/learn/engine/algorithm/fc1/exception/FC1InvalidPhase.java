package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception;

public class FC1InvalidPhase extends FC1AlgorithmException {

	public FC1InvalidPhase() {
		super("Internal Excpetion. Invalid Phase status.");
	}
}
