package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config.FC1Properties;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;

@Entity
@Table(schema = "core", name = "fc1_card_ratings")
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FC1CardRating implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "first_revision")
	private LocalDate firstRevision;
	
	@Column(name =  "last_revision")
	private LocalDate lastRevision;
	
	@Column(name = "next_revision")
	private LocalDate nextRevision;
	
	@Column(name = "interval")
	private Integer interval;
	
	@Column(name =  "easiness_factor")
	private Double easinessFactor;
	
	@Column(name =  "repetition")
	private Integer repetition;
	
	@Column(name = "card_id")
	private Long cardId;
	
	@Column(name =  "tally_id")
	private Long tallyId;
	
	@Column(name = "active", nullable = true)
	@Builder.Default
	private boolean active = true;
	
	public FC1CardRating(Card card) {
		this.cardId = card.getId();
		this.tallyId = card.getTally().getId();
	}
	
	public void incRepetition() {
		if(repetition == null) {
			repetition = 1;
		} else {
			repetition++;
		}
	}
	
	public void initRepetition() {
		repetition = 1;
	}
	
	public void decEasinessFactor(Double value) {
		setEasinessFactor(easinessFactor - value);
	}
	
	public void incEasinessFactor(Double value) {
		setEasinessFactor(easinessFactor + value);
	}
	
	public void setEasinessFactor(Double value) {
		if(value > FC1Properties.EF_MAX) {
			this.easinessFactor = FC1Properties.EF_MAX;
		} else if(value <= FC1Properties.EF_MIN) {
			this.easinessFactor = FC1Properties.EF_MIN;
		} else {
			this.easinessFactor = value;
		}
		
		this.easinessFactor = scaleEasinessFactor(easinessFactor);
	}
	
	private Double scaleEasinessFactor(Double value) {
		return BigDecimal.valueOf(value)
	    	.setScale(2, RoundingMode.HALF_UP)
	    	.doubleValue();
	}
	
	public void setInterval(Integer value) {
		if(value < 1) {
			interval = 1;
		} else if(value > FC1Properties.INTERVAL_MAX) {
			value = FC1Properties.INTERVAL_MAX;
		} else {
			interval = value;
		}
	}

	/*
	 * Set nextRevision n days from now
	 */
	public void setNextRevision(int days) {
		this.nextRevision = LocalDate.now().plusDays(days);
	}
	
	public void setNextRevision(LocalDate nextRevision) {
		this.nextRevision = nextRevision;
	}
	
	public CardMetric getMetric() {
		CardMetric metric = new CardMetric();
		
		metric.cardId = cardId;
		metric.firstRevision = this.firstRevision;
		metric.lastRevision = lastRevision;
		metric.nextRevision = nextRevision;
		
		return metric;
	}
}
