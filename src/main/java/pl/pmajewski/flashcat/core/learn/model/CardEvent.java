package pl.pmajewski.flashcat.core.learn.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.pmajewski.flashcat.core.card.model.Card;

@Entity
@Table(name = "cards_events", schema = "core")
@Data
@ToString(exclude = "card")
@EqualsAndHashCode(of = "id")
public class CardEvent {
	
	public enum Event {LEARN, QUIZ}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "card_id")
	private Card card;
	
	@Column
	@Enumerated(EnumType.STRING)
	private Event event;
	
	@Column
	private String value;
	
	@Column(name = "insert_timestamp", nullable = false)
	private LocalDateTime insertTimestamp;
	
	public CardEvent() { }
	
	public CardEvent(Card card, Event event) {
		this.card = card;
		this.event = event;
	}
	
	public CardEvent(Card card, Event event, String value) {
		this.card = card;
		this.event = event;
		this.value = value;
	}
	
	@PrePersist
	private void prePersist() {
		this.insertTimestamp = LocalDateTime.now();
	}
}