package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1Session;
import pl.pmajewski.flashcat.core.tally.model.Tally;

public interface FC1SessionRepository extends CrudRepository<FC1Session, Long> {

	Optional<FC1Session> findByTally(Tally tally);
	
}
