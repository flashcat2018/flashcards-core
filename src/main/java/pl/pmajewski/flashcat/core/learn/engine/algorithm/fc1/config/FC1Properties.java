package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config;

public class FC1Properties {

	public static final int INTERVAL_INIT = 1;
	public static final int INTERVAL_MAX = 128;
	public static final int INTERVAL_MULTIPLICATOR = 2;
	public static final double EF_INIT = 2.0d;
	public static final double EF_MAX = 4d;
	public static final double EF_MIN = 0.1d;
	public static final double EF_SUCCESS_BONUS = 0.1d;
	public static final double EF_FAIL_BONUS = 0.1d;
	public static final int NOVELTY_BUCKET_SIZE = 20;
}
