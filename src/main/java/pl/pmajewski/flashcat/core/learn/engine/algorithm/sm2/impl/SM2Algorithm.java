package pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.impl;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardJudgePojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.engine.LearnAlgorithm;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.exception.OperationNotSupported;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.sm2.repository.SM2CardOutcomeRepository;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;
import pl.pmajewski.flashcat.core.learn.exception.LearnException;

@Transactional
@Component
@ConditionalOnProperty(name =  "learn_algorithm", havingValue = "sm2")
public class SM2Algorithm extends LearnAlgorithm {

	private SM2Calculator calculator;
	private SM2Selector selector;
	
	public SM2Algorithm(SM2CardOutcomeRepository cardOutcomeRepo, CardRepository cardRepository) {
		calculator = new SM2Calculator(cardOutcomeRepo);
		selector = new SM2Selector(cardOutcomeRepo, cardRepository);
	}
	
	@Override
	public void judge(CardJudgePojo body) throws LearnException {
		calculator.judge(body);
	}

	@Override
	public Optional<Long> selectNextCard(Long tallyId) throws LearnException {
		return selector.nextCard(tallyId);
	}

	@Override
	public void resetCard(Card card) throws LearnException {
//		throw new OperationNotSupported("ResetCard");
	}

	@Override
	public void initCard(Card card) throws LearnException {
//		throw new OperationNotSupported("InitCard");
	}

	@Override
	public CardMetric getMetric(Card card) throws LearnException {
		throw new OperationNotSupported("Metric");
	}

	@Override
	public void deleteCard(Long cardId) throws LearnException {
		throw new OperationNotSupported("DeleteCard");
	}

	@Override
	public void deactivateCard(Long cardId) throws LearnException {
		throw new OperationNotSupported("DeactivateCard");
	}

	@Override
	public void activateCard(Long cardId) throws LearnException {
		throw new OperationNotSupported("ActivateCard");		
	}

	@Override
	public void move(Long cardId, Long tallyId) throws LearnException {
		throw new OperationNotSupported("MoveCard");
	}
}
