package pl.pmajewski.flashcat.core.metric.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class NoveltiesMetricDTO {

	private LocalDate date;
	private Long novelties;

}
