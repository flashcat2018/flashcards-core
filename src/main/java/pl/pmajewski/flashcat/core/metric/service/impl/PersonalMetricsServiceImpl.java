package pl.pmajewski.flashcat.core.metric.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.pojo.CardsMetricDTO;
import pl.pmajewski.flashcat.core.card.service.CardMetricsService;
import pl.pmajewski.flashcat.core.learn.service.LearnHistoryMetrics;
import pl.pmajewski.flashcat.core.metric.dto.MetricsContainerDTO;
import pl.pmajewski.flashcat.core.metric.service.PersonalMetricsService;
import pl.pmajewski.flashcat.core.tally.utils.DateRange;

@Service
@Primary
@Transactional
public class PersonalMetricsServiceImpl implements PersonalMetricsService {

	private LearnHistoryMetrics eventService;
	private CardMetricsService cardMetrics;
	
	public PersonalMetricsServiceImpl(LearnHistoryMetrics eventService, CardMetricsService cardMetrics) {
		this.eventService = eventService;
		this.cardMetrics = cardMetrics;
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> repetitions(Long accountId, LocalDate from, LocalDate to) {
		List<CardsMetricDTO> result = new ArrayList<>();
		Map<LocalDate, Long> remaindedCards = eventService.accountRepetitions(accountId, from, to);
		for(LocalDate date : DateRange.between(from, to)) {
			if(remaindedCards.containsKey(date)) {
				result.add(new CardsMetricDTO(date, remaindedCards.get(date)));
			} else {
				result.add(new CardsMetricDTO(date, 0l));
			}
		}
		
		Collections.sort(result, Comparator.comparing(CardsMetricDTO::getDate).reversed());
		return new MetricsContainerDTO<>(result);
	}
	
	@Override
	public MetricsContainerDTO<CardsMetricDTO> novelties(Long accountId, LocalDate from, LocalDate to) {
		List<CardsMetricDTO> result = new ArrayList<>();
		Map<LocalDate, Long> remaindedCards = eventService.accountNovelties(accountId, from, to);
		for(LocalDate date : DateRange.between(from, to)) {
			if(remaindedCards.containsKey(date)) {
				result.add(new CardsMetricDTO(date, remaindedCards.get(date)));
			} else {
				result.add(new CardsMetricDTO(date, 0l));
			}
		}
		
		Collections.sort(result, Comparator.comparing(CardsMetricDTO::getDate).reversed());
		return new MetricsContainerDTO<>(result);
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> calendar(Long accountId, LocalDate from, LocalDate to) {
		Map<LocalDate, Long> metrics = cardMetrics.calendarAccount(accountId, from, to);
		List<CardsMetricDTO> result = new LinkedList<>();
		for (LocalDate date : DateRange.between(from, to)) {
			if(metrics.get(date) == null) {
				result.add(new CardsMetricDTO(date, 0l));
			} else {
				result.add(new CardsMetricDTO(date, metrics.get(date)));
			}
		}
		
		Collections.sort(result, Comparator.comparing(CardsMetricDTO::getDate).reversed());
		return new MetricsContainerDTO<>(result); 
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> addedCards(Long accountId, LocalDate from, LocalDate to) {
		Map<LocalDate, Long> metrics = cardMetrics.countAddedCards(accountId, from, to);
		List<CardsMetricDTO> result = new LinkedList<>();
		for(LocalDate date: DateRange.between(from, to)) {
			if(!metrics.containsKey(date)) {
				result.add(new CardsMetricDTO(date, 0l));
			} else {
				result.add(new CardsMetricDTO(date, metrics.get(date)));
			}
		}
		
		Collections.sort(result, Comparator.comparing(CardsMetricDTO::getDate).reversed());
		return new MetricsContainerDTO<>(result);
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> learnTime(Long accountId, LocalDate from, LocalDate to) {
		Map<LocalDate, Long> metrics = eventService.learnTime(accountId, from, to);
		List<CardsMetricDTO> result = new LinkedList<>();
		for(LocalDate date: DateRange.between(from, to)) {
			if(metrics.containsKey(date)) {
				result.add(new CardsMetricDTO(date, metrics.get(date)));
			} else {
				result.add(new CardsMetricDTO(date, 0l));
			}
		}
		
		Collections.sort(result, Comparator.comparing(CardsMetricDTO::getDate).reversed());
		return new MetricsContainerDTO<>(result);
	}
}
