package pl.pmajewski.flashcat.core.metric.controller;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.pmajewski.flashcat.core.card.pojo.CardsMetricDTO;
import pl.pmajewski.flashcat.core.metric.dto.MetricsContainerDTO;
import pl.pmajewski.flashcat.core.metric.service.PersonalMetricsService;
import pl.pmajewski.flashcat.core.metric.service.impl.PersonalMetricsAccessService;

@RestController
public class MetricsController {
	
	private PersonalMetricsService personalMetricsService;

	public MetricsController(PersonalMetricsAccessService personalMetricsService) {
		this.personalMetricsService = personalMetricsService;
	}
	
	@GetMapping(value = "accounts/{accountId}/metrics/repetitions")
	public MetricsContainerDTO<CardsMetricDTO> repetitions(@PathVariable Long accountId, @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate from, 
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
		
		return personalMetricsService.repetitions(accountId, from, to);
	}
	
	@GetMapping(value = "accounts/{accountId}/metrics/novelties")
	public MetricsContainerDTO<CardsMetricDTO> nevelties(@PathVariable Long accountId, @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate from, 
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
		
		return personalMetricsService.novelties(accountId, from, to);
	}
	
	@GetMapping(value = "accounts/{accountId}/metrics/calendar")
	public MetricsContainerDTO<CardsMetricDTO> calendar(@PathVariable Long accountId, @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate from,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
		if(from.isAfter(to)) {
			from = to;
		}
		
		return personalMetricsService.calendar(accountId, from, to);
	}
	
	@GetMapping(value = "accounts/{accountId}/metrics/addedCards")
	public MetricsContainerDTO<CardsMetricDTO> addedCards(@PathVariable Long accountId, @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate from,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
		
		return personalMetricsService.addedCards(accountId, from, to);
	}
	
	/*
	 * Return learn time in second
	 */
	@GetMapping(value = "accounts/{accountId}/metrics/learnTime")
	public MetricsContainerDTO<CardsMetricDTO> learnTime(@PathVariable Long accountId, @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate from,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
		
		return personalMetricsService.learnTime(accountId, from, to);
	}
}
