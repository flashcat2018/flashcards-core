package pl.pmajewski.flashcat.core.metric.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.auth.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.core.card.pojo.CardsMetricDTO;
import pl.pmajewski.flashcat.core.metric.dto.MetricsContainerDTO;
import pl.pmajewski.flashcat.core.metric.dto.NoveltiesMetricDTO;
import pl.pmajewski.flashcat.core.metric.dto.RepetitionMetricDTO;
import pl.pmajewski.flashcat.core.metric.service.PersonalMetricsService;

@Service
public class PersonalMetricsAccessService implements PersonalMetricsService {

	private PersonalMetricsService metricsService;
	
	public PersonalMetricsAccessService(PersonalMetricsService metricsService) {
		this.metricsService = metricsService;
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> repetitions(Long accountId, LocalDate from, LocalDate to) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return metricsService.repetitions(accountId, from, to);
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> novelties(Long accountId, LocalDate from, LocalDate to) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return metricsService.novelties(accountId, from, to);
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> calendar(Long accountId, LocalDate from, LocalDate to) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return metricsService.calendar(accountId, from, to);
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> addedCards(Long accountId, LocalDate from, LocalDate to) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return metricsService.addedCards(accountId, from, to);
	}

	@Override
	public MetricsContainerDTO<CardsMetricDTO> learnTime(Long accountId, LocalDate from, LocalDate to) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return metricsService.learnTime(accountId, from, to);
	}
}
