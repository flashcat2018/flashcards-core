package pl.pmajewski.flashcat.core.metric.service;

import java.time.LocalDate;

import pl.pmajewski.flashcat.core.card.pojo.CardsMetricDTO;
import pl.pmajewski.flashcat.core.metric.dto.MetricsContainerDTO;

public interface PersonalMetricsService {

	MetricsContainerDTO<CardsMetricDTO> repetitions(Long accountId, LocalDate from, LocalDate to);
	
	MetricsContainerDTO<CardsMetricDTO> novelties(Long accountId, LocalDate from, LocalDate to);
	
	MetricsContainerDTO<CardsMetricDTO> calendar(Long accountId, LocalDate from, LocalDate to);
	
	MetricsContainerDTO<CardsMetricDTO> addedCards(Long accountId, LocalDate from, LocalDate to);
	
	MetricsContainerDTO<CardsMetricDTO> learnTime(Long accountId, LocalDate from, LocalDate to);
}
