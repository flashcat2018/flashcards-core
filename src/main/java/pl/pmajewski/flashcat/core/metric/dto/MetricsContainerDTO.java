package pl.pmajewski.flashcat.core.metric.dto;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MetricsContainerDTO<T> {

	private Collection<T> data;
}