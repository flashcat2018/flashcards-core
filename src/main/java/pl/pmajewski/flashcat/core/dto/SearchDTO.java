package pl.pmajewski.flashcat.core.dto;

import java.util.List;

import lombok.ToString;

@ToString
public class SearchDTO <T> {

	public Long totalNumber;
	public Integer offset;
	public Integer limit;
	public List<T> data;
	
	public SearchDTO(List<T> data, Long count, Integer offset, Integer limit) {
		this.totalNumber = count;
		this.data = data;
		this.offset = offset;
		this.limit = limit;
	}
}
