package pl.pmajewski.flashcat.core.card.service;

import java.time.LocalDate;
import java.util.Map;

public interface CardMetricsService {

	Long tallyRepetitions(Long tallyId);
	
	Long tallyNovelties(Long tallyId);
	
	Long countAll(Long tallId);
	
	Map<LocalDate, Long> calendarAccount(Long accountId, LocalDate from, LocalDate to);
	
	Map<LocalDate, Long> countAddedCards(Long accountId, LocalDate from, LocalDate to);
}
