package pl.pmajewski.flashcat.core.card.service.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.auth.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.core.card.pojo.CardDescPojo;
import pl.pmajewski.flashcat.core.card.service.CardSearchService;
import pl.pmajewski.flashcat.core.dto.SearchDTO;
import pl.pmajewski.flashcat.core.tally.model.Tally.Access;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;

@Service
@Primary
public class CardSearchAccessService implements CardSearchService {

	private CardSearchService cardService;
	private TallyRepository tallyRepo;
	
	public CardSearchAccessService(CardSearchServiceImpl cardService, TallyRepository tallyRepository) {
		this.cardService = cardService;
		this.tallyRepo = tallyRepository;
	}

	@Override
	public SearchDTO<CardDescPojo> searchQuestions(Long tallyId, String query, Integer limit, Integer offset) {
		tallyRepo.findById(tallyId).ifPresent(tally -> {
			if(tally.getAccess().equals(Access.PRIVATE)
					&& !Auth.userId().equals(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return cardService.searchQuestions(tallyId, query, limit, offset);
	}
}
