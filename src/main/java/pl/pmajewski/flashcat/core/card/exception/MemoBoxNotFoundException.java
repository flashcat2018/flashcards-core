package pl.pmajewski.flashcat.core.card.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class MemoBoxNotFoundException extends RuntimeException {

	public MemoBoxNotFoundException() {
		super();
	}

	public MemoBoxNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MemoBoxNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public MemoBoxNotFoundException(String message) {
		super(message);
	}

	public MemoBoxNotFoundException(Throwable cause) {
		super(cause);
	}
}
