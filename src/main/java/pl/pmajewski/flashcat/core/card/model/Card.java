package pl.pmajewski.flashcat.core.card.model;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.pmajewski.flashcat.core.card.exception.InvalidAnswerPartException;
import pl.pmajewski.flashcat.core.card.exception.InvalidQuestionPartException;
import pl.pmajewski.flashcat.core.card.pojo.AnswerPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardDescPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;
import pl.pmajewski.flashcat.core.learn.model.CardEvent;
import pl.pmajewski.flashcat.core.tally.model.Tally;

@Entity
@Table(name = "cards", schema = "core")
@Data
@EqualsAndHashCode(of = "id")
@ToString(exclude = {"tally", "events", "answers", "cardMetric"})
public class Card {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "create_timestamp", updatable = false)
	private LocalDateTime createTimestamp;
	
	@Column(name = "update_timestamp")
	private LocalDateTime updateTimestamp;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tally_id")
	private Tally tally;
	
	@Column(name = "active", nullable = false)
	private boolean active = true;
	
	// OneToMany - just performance opt
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "card", cascade = { CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}, orphanRemoval = true)
	private Set<QuestionCard> question; 
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "card", cascade = { CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH }, orphanRemoval = true)
	private Set<AnswerCard> answers;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "card", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Set<CardEvent> events;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "card")
	@Getter(value = AccessLevel.PRIVATE)
	@Setter(value = AccessLevel.PRIVATE)
	private Set<Metric> cardMetric;
	
	public Card() {
		this.cardMetric = new HashSet<>();
		this.cardMetric.add(new Metric(this));
	}
	
	public Card(Long id) {
		this();
		this.id = id;
	}
	
	public Metric getMetric() {
		if(this.cardMetric.isEmpty()) {
			return null;
		}
		
		return this.cardMetric.toArray(Metric[]::new)[0];
	}
	
	public void updateMetric(CardMetric cardMetricDTO) {
		Metric metric = getMetric();
		metric.update(cardMetricDTO);
	}
	
	public void setMetric(Metric metric) {
		if(!this.cardMetric.isEmpty()) {
			this.cardMetric.clear();
		}
		
		this.cardMetric.add(metric);
	}
	
	@PreUpdate
	private void preUpdate() {
		this.updateTimestamp = LocalDateTime.now();
	}
	
	@PrePersist
	private void prePersist() {
		this.createTimestamp = LocalDateTime.now();
		this.updateTimestamp = LocalDateTime.now();
	}
	
	public CardPojo getPojo() {
		CardPojo p = new CardPojo();
		
		p.setId(id);
		p.setTallyId(this.tally.getId());
		p.setActive(this.isActive());
		p.setCreateTimestamp(this.createTimestamp);
		p.setUpdateTimestamp(this.updateTimestamp);

		if(question.isEmpty()) {
			throw new InvalidQuestionPartException("Question part is missing.");
		} else {
			p.setQuestion(this.question.stream().findFirst().get().getPojo());
		}
		
		if(answers.isEmpty()) {
			throw new InvalidAnswerPartException("Answer part is missing.");
		} else {
			AnswerPojo[] answersDTO = answers.stream()
				.sorted(Comparator.comparing(CardPart::getSequenceId))
				.map(AnswerCard::getPojo)
				.toArray(AnswerPojo[]::new);
			p.setAnswers(answersDTO);
		}
		
		return p;
	}
	
	public CardDescPojo getDescPojo() {
		CardDescPojo p = new CardDescPojo();
		
		p.setId(id);
		p.setTallyId(tally.getId());
		p.setActive(active);
		p.setCreateTimestamp(createTimestamp);
		p.setUpdateTimestamp(updateTimestamp);

		if(question.isEmpty()) {
			throw new InvalidQuestionPartException("Question part is missing.");
		} else {
			p.setQuestion(this.question.stream().findFirst().get().getPojo());
		}
		
		return p;
	}
}