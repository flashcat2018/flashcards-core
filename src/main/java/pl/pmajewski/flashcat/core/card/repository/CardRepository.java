package pl.pmajewski.flashcat.core.card.repository;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import pl.pmajewski.flashcat.core.card.model.Card;

public interface CardRepository extends PagingAndSortingRepository<Card, Long>, CardCustomRepository {
	
	List<Card> findByTallyId(Long tallyId);
	
	@Query("select count(*) from Card c where c.tally.id = :tallyId")
	Long countAll(@Param("tallyId") Long tallyId);
	
	@Query("select c from Card c where c.tally.id = :tallyId")
	Stream<Card> streamPure(@Param("tallyId") Long tallyId);
	
	@Query("select c from Card c join fetch c.question where c.tally.id = :tallyId")
	Stream<Card> streamWithParts(@Param("tallyId") Long tallyId);
	
	@Query("select c from Card c join fetch c.cardMetric where c.tally.id = :tallyId")
	Stream<Card> streamWithMetric(@Param("tallyId") Long tallyId);
	
	@Query("select c from Card c join fetch c.cardMetric where c.tally.accountId = :account")
	Stream<Card> streamWithMetricByAccount(@Param("account") Long accountId);
}
