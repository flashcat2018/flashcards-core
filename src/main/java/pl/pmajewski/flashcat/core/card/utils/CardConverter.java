package pl.pmajewski.flashcat.core.card.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.core.card.exception.CardExternalException;
import pl.pmajewski.flashcat.core.card.model.AnswerCard;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.model.CardPart.ContentType;
import pl.pmajewski.flashcat.core.card.model.QuestionCard;
import pl.pmajewski.flashcat.core.card.pojo.AnswerPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.pojo.QuestionPojo;

@Component
public class CardConverter {

	public Card toCard(CardPojo pojo) {
		QuestionCard question = createQuestionCard(pojo.getQuestion());
		List<AnswerCard> answers = Arrays.stream(pojo.getAnswers()).map(i -> createAnswerCard(i))
				.collect(Collectors.toList());

		Card card = new Card();
		card.setId(pojo.getId());
		card.setQuestion(new HashSet<>(Arrays.asList(question)));
		card.setAnswers(new HashSet<>(answers));

		question.setCard(card);
		answers.stream().forEach(i -> i.setCard(card));

		return card;
	}

	public QuestionCard createQuestionCard(QuestionPojo pojo) {
		QuestionCard question = new QuestionCard();

		question.setId(pojo.getId());
		question.setSequenceId(0l);
		question.setContent(pojo.getContent());
		question.setContentType(ContentType.valueOf(pojo.getContentType()));

		return question;
	}

	public AnswerCard createAnswerCard(AnswerPojo pojo) {
		AnswerCard answer = new AnswerCard();

		answer.setId(pojo.getId());
		answer.setSequenceId(pojo.getSequenceId());
		answer.setContentType(ContentType.valueOf(pojo.getContentType()));
		answer.setContent(pojo.getContent());

		return answer;
	}

	public QuestionCard updateQuestionCard(QuestionPojo pojo, QuestionCard question) {
		if (!pojo.getId().equals(question.getId())) {
			throw new CardExternalException("Can't update card because id's are incompatibile");
		}
		question.setSequenceId(pojo.getSequenceId());
		question.setContentType(ContentType.valueOf(pojo.getContentType()));
		question.setContent(pojo.getContent());

		return question;
	}

	public AnswerCard updateAnswerCard(AnswerPojo pojo, AnswerCard answer) {
		if (!pojo.getId().equals(answer.getId())) {
			throw new CardExternalException("Can't update card because id's are incompatibile");
		}
		answer.setSequenceId(pojo.getSequenceId());
		answer.setContentType(ContentType.valueOf(pojo.getContentType()));
		answer.setContent(pojo.getContent());

		return answer;
	}

	public Set<QuestionCard> updateQuestionCards(QuestionPojo pojo, Set<QuestionCard> questions) {
		return updateQuestionCards(Arrays.asList(pojo), questions);
	}

	public Set<QuestionCard> updateQuestionCards(List<QuestionPojo> pojo, Set<QuestionCard> questions) {
		return pojo.stream().sorted((o1, o2) -> {
			if (o1.getSequenceId() > o2.getSequenceId()) {
				return 1;
			} else if (o1.getSequenceId() < o2.getSequenceId()) {
				return -1;
			} else {
				return 0;
			}
		}).map(i -> {
			if (i.getId() != null && questions.contains(new QuestionCard(i.getId()))) {
				QuestionCard question = findQuestionCard(i.getId(), questions);
				return updateQuestionCard(i, question);
			} else {
				return createQuestionCard(i);
			}
		}).collect(Collectors.toSet());
	}

	public Set<AnswerCard> updateAnswerCards(List<AnswerPojo> pojo, Set<AnswerCard> questions) {
		return pojo.stream().sorted((o1, o2) -> {
			if (o1.getSequenceId() > o2.getSequenceId()) {
				return 1;
			} else if (o1.getSequenceId() < o2.getSequenceId()) {
				return -1;
			} else {
				return 0;
			}
		}).map(i -> {
			if (i.getId() != null && questions.contains(new AnswerCard(i.getId()))) {
				AnswerCard question = findAnswerCard(i.getId(), questions);
				return updateAnswerCard(i, question);
			} else {
				return createAnswerCard(i);
			}
		}).collect(Collectors.toSet());
	}

	private QuestionCard findQuestionCard(Long id, Set<QuestionCard> list) {
		return list.stream().filter(i -> i.getId().equals(id)).findFirst().get();
	}

	private AnswerCard findAnswerCard(Long id, Set<AnswerCard> list) {
		return list.stream().filter(i -> i.getId().equals(id)).findFirst().get();
	}
}
