package pl.pmajewski.flashcat.core.card.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardJudgePojo {
	
	private Long cardId;
	private String event; // YES or NO
}
