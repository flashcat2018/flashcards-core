package pl.pmajewski.flashcat.core.card.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CardNotFoundException extends RuntimeException {

	public CardNotFoundException() {
		super();
	}

	public CardNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CardNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CardNotFoundException(String message) {
		super(message);
	}

	public CardNotFoundException(Throwable cause) {
		super(cause);
	}
}
