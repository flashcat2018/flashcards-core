package pl.pmajewski.flashcat.core.card.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.data.domain.Pageable;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.repository.CardCustomRepository;

public class CardCustomRepositoryImpl implements CardCustomRepository {
	
	private EntityManager entityManager;
	
	public CardCustomRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public Long countQuestions(Long tallyId, List<String> keywords) {
		String queryStr = "select count(*) from Card c join c.question cq where c.tally.id = :id "+keywordToQueryPart(keywords);
		Query query = entityManager.createQuery(queryStr);
		query.setParameter("id", tallyId);
		fillQueryWithKeywords(query, keywords);
		
		List<Long> result = query.getResultList();
		return result.get(0);
	}

	@Override
	public List<Card> searchQuestions(Long tallyId, List<String> keywords, Pageable page) {
		String queryStr = "select c from Card c join fetch c.question cq where c.tally.id = :id "+keywordToQueryPart(keywords)+" order by c.updateTimestamp desc";
		Query query = entityManager.createQuery(queryStr);
		query.setParameter("id", tallyId);
		fillQueryWithKeywords(query, keywords);
		Long offset = page.getOffset();
		query.setFirstResult(offset.intValue());
		query.setMaxResults(page.getPageSize());
		
		List resultList = query.getResultList();
		return resultList;
	}
	
	private void fillQueryWithKeywords(Query query, List<String> keywords) {
		for(int i=0; i < keywords.size(); i++) {
			query.setParameter("keyword"+i, "%"+keywords.get(i)+"%");
		}
	}

	private String keywordToQueryPart(List<String> keywords) {
		StringBuilder sb = new StringBuilder();
			
		for(int i=0; i < keywords.size(); i++) {
			sb.append("and upper(cq.content) ")
			.append("like upper(")
			.append(":keyword").append(i)
			.append(") ");
		}
		
		return sb.toString();
	}
}
