package pl.pmajewski.flashcat.core.card.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.pojo.CardDescPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPatchPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.service.CardSearchService;
import pl.pmajewski.flashcat.core.card.service.CardService;
import pl.pmajewski.flashcat.core.card.service.impl.CardServiceAccessProxy;
import pl.pmajewski.flashcat.core.dto.SearchDTO;

@RestController
@CommonsLog
public class CardController {
		
	private Auth auth;
	private CardService cardService;
	private CardSearchService cardSearchService;
	
	public CardController(CardServiceAccessProxy cardService, Auth auth, CardSearchService cardSeachService) {
		this.cardService = cardService;
		this.auth = auth;
		this.cardSearchService = cardSeachService;
	}
	
	@GetMapping(value = "/cards/{id}")
	public CardPojo getCard(@PathVariable Long id) {
		log.info("["+auth.getUserId()+"] load card by id: "+id);
		return cardService.getCardById(id);
	}
	
	@PostMapping(value = "/tallies/{id}/cards")
	public CardPojo addCard(@PathVariable Long id, @RequestBody CardPojo card) {
		return cardService.addCard(id, card);
	}
	
	@PutMapping(value = "/cards")
	public CardPojo updateCard(@RequestBody CardPojo card) {
		return cardService.updateCard(card);
	}
	
	@DeleteMapping(value = "/cards/{id}")
	public void deleteCard(@PathVariable Long id) {
		log.info("["+auth.getUserId()+"] Remove card id: "+id);
		cardService.deleteCard(id);
	}
	
	@PatchMapping(value = "cards/{id}")
	public void changeState(@PathVariable Long id, @RequestBody CardPatchPojo body) {
		log.info("["+auth.getUserId()+"] Patch card id: "+id);
		cardService.patchCard(id, body);
	}
	
	@GetMapping(value = "/tallies/{id}/cards/search")
	public SearchDTO<CardDescPojo> searchQuestions(@PathVariable Long id, @RequestParam(required = false, defaultValue = "") String query, 
			@RequestParam(required = false, defaultValue = "0")  Integer limit, @RequestParam(required = false, defaultValue = "0") Integer offset) {
		log.info("["+auth.getUserId()+"] List cards for tallyId: "+id+" by query: "+query);
		return cardSearchService.searchQuestions(id, query, limit, offset);
	}
}
