package pl.pmajewski.flashcat.core.card.service.image;

import java.awt.Dimension;

import pl.pmajewski.flashcat.core.card.utils.ImageUtils;

public interface Image {
	
	byte[] getByte();
	
	String getMimeFormat();
	
	int getWidth();
	
	int getHeight();
	
	default Dimension getDimension() {
		return new Dimension(getWidth(), getHeight());
	}
	
	default String getImageFormat() {
		return ImageUtils.mimeToImageFormat(getMimeFormat());
	}
}
