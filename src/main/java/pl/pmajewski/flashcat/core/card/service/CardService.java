package pl.pmajewski.flashcat.core.card.service;

import pl.pmajewski.flashcat.core.card.pojo.CardPatchPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;

public interface CardService {

	CardPojo getCardById(Long cardId);
	
	CardPojo addCard(Long tallyId, CardPojo cardPojo);
	
	CardPojo updateCard(CardPojo cardPojo);
	
	void deleteCard(Long cardId);
	
	void patchCard(Long cardId, CardPatchPojo card);
}
