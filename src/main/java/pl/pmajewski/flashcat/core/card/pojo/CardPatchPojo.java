package pl.pmajewski.flashcat.core.card.pojo;

import lombok.Data;

@Data
public class CardPatchPojo {

	private Long tallyId;
	private Boolean active;

}