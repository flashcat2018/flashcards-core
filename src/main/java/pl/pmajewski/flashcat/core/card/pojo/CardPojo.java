package pl.pmajewski.flashcat.core.card.pojo;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CardPojo {

	private Long id;
	private Long tallyId;
	private Boolean active;
	private LocalDateTime createTimestamp;
	private LocalDateTime updateTimestamp;
	private LocalDateTime repeatTimestamp;
	private QuestionPojo question;
	private AnswerPojo[] answers;
}
