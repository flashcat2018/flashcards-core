package pl.pmajewski.flashcat.core.card.service.impl;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.exception.CardNotFoundException;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardPatchPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.card.service.CardService;
import pl.pmajewski.flashcat.core.card.utils.CardConverter;
import pl.pmajewski.flashcat.core.learn.engine.LearnEngine;
import pl.pmajewski.flashcat.core.tally.exception.TallyNotFoundException;
import pl.pmajewski.flashcat.core.tally.model.Tally;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;

@Service
@Transactional
public class CardServiceImpl implements CardService {

	private CardRepository cardRepository;
	private TallyRepository tallyRepository;
	private LearnEngine learnEngine;
	
	@Autowired
	private CardConverter converter;
	
	public CardServiceImpl(CardRepository cardRepository, TallyRepository tallyRepository, LearnEngine learnAlgorithm) {
		this.cardRepository = cardRepository;
		this.tallyRepository = tallyRepository;
		this.learnEngine = learnAlgorithm;
	}

	@Override
	public CardPojo getCardById(Long cardId) {
		return cardRepository.findById(cardId)
			.orElseThrow(() -> new CardNotFoundException("Card with id: "+cardId+" not found."))
			.getPojo();
	}
	
	@Override
	public CardPojo addCard(Long tallyId, CardPojo cardPojo) {
		Card card = converter.toCard(cardPojo);
		card.setTally(new Tally(tallyId));
		card = cardRepository.save(card);
		updateTally(tallyId);
		learnEngine.initCard(card);
		return card.getPojo();
	}
	
	private void updateTally(Long tallyId) {
		Tally tally = tallyRepository.findById(tallyId).get();
		tally.setLastUpdateTimestamp(LocalDateTime.now());
		tallyRepository.save(tally);
	}

	@Override
	public CardPojo updateCard(CardPojo cardPojo) {
		Card card = cardRepository.findById(cardPojo.getId())
				.orElseThrow(() -> new CardNotFoundException("Card with id: "+cardPojo.getId()+" not found."));
		card.getQuestion().clear();
		card.getQuestion().addAll(converter.updateQuestionCards(cardPojo.getQuestion(), card.getQuestion()));
		card.getQuestion().stream().forEach(i -> {
			i.setCard(card);
		});
		card.getAnswers().clear();
		card.getAnswers().addAll(converter.updateAnswerCards(Arrays.asList(cardPojo.getAnswers()), card.getAnswers()));
		card.getAnswers().stream().forEach(i -> {
			i.setCard(card);
		});
		cardRepository.save(card);
		return card.getPojo();
	}

	@Override
	public void deleteCard(Long cardId) {
		learnEngine.deleteCard(cardId);
		cardRepository.deleteById(cardId);
	}

	@Override
	public void patchCard(Long cardId, CardPatchPojo body) {
		Card card = cardRepository.findById(cardId)
				.orElseThrow(() -> new CardNotFoundException("Card with id: "+cardId+" not found."));
		
		if(body.getActive() != null) {
			changeCardState(card, body.getActive());
		}
		
		if(body.getTallyId() != null) {
			moveTally(card, body.getTallyId());
		}
		
		cardRepository.save(card);
	}
	
	private void moveTally(Card card, Long tallyId) {
		Tally tally = tallyRepository.findById(tallyId)
			.orElseThrow(() -> new TallyNotFoundException("Tally with id="+tallyId+" not found"));
		
		learnEngine.move(card.getId(), tallyId);
		card.setTally(tally);
	}

	private void changeCardState(Card card, boolean state) {
		if(state) {
			learnEngine.activateCard(card.getId());
			card.setActive(true);
		} else {
			learnEngine.deactivateCard(card.getId());
			card.setActive(false);
		}
	}
}
