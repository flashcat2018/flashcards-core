package pl.pmajewski.flashcat.core.card.pojo;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardDescPojo {

	private Long id;
	private Long tallyId;
	private Boolean active;
	private LocalDateTime createTimestamp;
	private LocalDateTime updateTimestamp;
	private QuestionPojo question;
	
}
