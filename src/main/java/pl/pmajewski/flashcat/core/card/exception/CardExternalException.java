package pl.pmajewski.flashcat.core.card.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class CardExternalException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public CardExternalException() {
		super();
	}

	public CardExternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CardExternalException(String message, Throwable cause) {
		super(message, cause);
	}

	public CardExternalException(String message) {
		super(message);
	}

	public CardExternalException(Throwable cause) {
		super(cause);
	}
}
