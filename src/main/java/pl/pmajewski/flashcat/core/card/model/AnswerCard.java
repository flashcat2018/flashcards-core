package pl.pmajewski.flashcat.core.card.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pmajewski.flashcat.core.card.pojo.AnswerPojo;

@Entity
@DiscriminatorValue("ANSWER")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class AnswerCard extends CardPart {
	
	public AnswerCard(Long id) {
		this.id = id;
	}
	
	public AnswerPojo getPojo() {
		AnswerPojo p = new AnswerPojo();
		
		p.setId(id);
		p.setSequenceId(sequenceId);
		p.setContentType(contentType.toString());
		p.setContent(content);
		
		return p;
	}
	
	@Override
	public String toString() {
		if(super.contentType.equals(ContentType.IMAGE)) {
			return ReflectionToStringBuilder.toStringExclude(this, new String[] {"card", "content"});
		}
		
		return ReflectionToStringBuilder.toStringExclude(this, "card"); 
	}
}
