package pl.pmajewski.flashcat.core.card.service;

import pl.pmajewski.flashcat.core.card.pojo.CardDescPojo;
import pl.pmajewski.flashcat.core.dto.SearchDTO;

public interface CardSearchService {

	SearchDTO<CardDescPojo> searchQuestions(Long tallyId, String query, Integer limit, Integer offset);
	
}
