package pl.pmajewski.flashcat.core.card.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import pl.pmajewski.flashcat.core.card.model.Card;

public interface CardCustomRepository {

	Long countQuestions(Long tallyId, List<String>  keywords);
	
	List<Card> searchQuestions(Long tallyId, List<String> query, Pageable page);
}
