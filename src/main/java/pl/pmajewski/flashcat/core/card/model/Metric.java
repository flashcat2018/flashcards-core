package pl.pmajewski.flashcat.core.card.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;

@Entity
@Table(name =  "cards_metric", schema = "core")
@Data
@EqualsAndHashCode(of =  "id")
@ToString(exclude = "card")
public class Metric {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "card_id")
	private Card card;
	
	@Column(name = "first_revision")
	private LocalDate firstRevision;
	
	@Column(name =  "last_revision")
	private LocalDate lastRevision;
	
	@Column(name =  "next_revision")
	private LocalDate nextRevision;
	
	/**
	 * Just for Hibernate compatibility
	 */
	private Metric() {
		// DON'T REMOVE
	}
	
	public Metric(Card card) {
		this.card = card;
	}
	
	public void update(CardMetric cardMetric) {
		this.firstRevision = cardMetric.firstRevision;
		this.lastRevision = cardMetric.lastRevision;
		this.nextRevision = cardMetric.nextRevision;
	}
}
