package pl.pmajewski.flashcat.core.card.pojo;

import lombok.Data;

@Data
public class QuestionPojo {
	
	private Long id;
	private Long sequenceId;
	private String contentType;
	private String content;
}
