package pl.pmajewski.flashcat.core.card.service.image;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Base64;

import javax.imageio.ImageIO;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.exception.ImageProcessingException;
import pl.pmajewski.flashcat.core.card.model.CardPart;
import pl.pmajewski.flashcat.core.card.pojo.AnswerPojo;

@Slf4j
public class ImageCardAdapter implements Image {
	
	private String base64Content;
	
	private ImageCardAdapter(String base64Content) {
		this.base64Content = base64Content;
	}
	
	public static ImageCardAdapter from(CardPart card) {
		if(!CardPart.ContentType.IMAGE.equals(card.getContentType())) {
			throw new ImageProcessingException("Invalid card content type");
		}
		
		return new ImageCardAdapter(card.getContent());
	}
	
	public static ImageCardAdapter from(AnswerPojo answer) {
		if(!CardPart.ContentType.IMAGE.toString().equals(answer.getContentType())) {
			throw new ImageProcessingException("Invalid card content type");
		}
		
		if(answer.getContent() == null || answer.getContent().isEmpty()) {
			throw new ImageProcessingException("Card content is null or empty");
		}
		
		return new ImageCardAdapter(answer.getContent());
	}

	@Override
	public byte[] getByte() {
		return Base64.getMimeDecoder().decode(base64Content.split(",")[1]);
	}

	@Override
	public String getMimeFormat() {
		try {
			String mimeType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(getByte()));
			if(mimeType == null) {
				mimeType = base64MimeType();
			}
			
			return mimeType;
		} catch (IOException e) {
			throw new ImageProcessingException(e);
		}
	}
	
	private String base64MimeType() {
		return base64Content.substring("data:".length(), base64Content.indexOf(";base64"));
	}


	@Override
	public int getWidth() {
		return getBI().getWidth();
	}

	@Override
	public int getHeight() {
		return getBI().getHeight();
	}
	
	private  BufferedImage getBI() {
		try (ByteArrayInputStream is = new ByteArrayInputStream(getByte())) {
			return ImageIO.read(is);
		} catch (IOException e) {
			log.error("["+Auth.userId()+"] Image prcessing exception, from base64 to BufferedImage", e);
			throw new ImageProcessingException(e);
		}
	}
}
