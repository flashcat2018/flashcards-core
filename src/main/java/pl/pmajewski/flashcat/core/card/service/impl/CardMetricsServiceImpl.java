package pl.pmajewski.flashcat.core.card.service.impl;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.card.service.CardMetricsService;

/**
 * Intended for use from other services.
 * Doesn't check account permission
 */
@Service
@Transactional
public class CardMetricsServiceImpl implements CardMetricsService {

	private CardRepository cardRepo;

	public CardMetricsServiceImpl(CardRepository cardRepo) {
		this.cardRepo = cardRepo;
	}

	@Override
	public Long tallyRepetitions(Long tallyId) {
		return cardRepo.streamWithMetric(tallyId)
			.filter(i -> i.getMetric().getFirstRevision() != null)
			.filter(i -> i.getMetric().getNextRevision() != null)
			.filter(i -> i.getMetric().getFirstRevision().isBefore(LocalDate.now()))
			.filter(i -> LocalDate.now().equals(i.getMetric().getNextRevision()) ||
					LocalDate.now().isAfter(i.getMetric().getNextRevision()))
			.count();
	}

	@Override
	public Long tallyNovelties(Long tallyId) {
		return cardRepo.streamWithMetric(tallyId)
			.filter(i -> i.getMetric().getFirstRevision() == null)
			.count();
	}

	@Override
	public Long countAll(Long tallId) {
		return cardRepo.countAll(tallId);
	}

	@Override
	public Map<LocalDate, Long> calendarAccount(Long accountId, LocalDate from, LocalDate to) {
		return countNextRevisions(cardRepo.streamWithMetricByAccount(accountId), from, to);
	}
	
	private Map<LocalDate, Long> countNextRevisions(Stream<Card> cards, LocalDate from, LocalDate to) {
		Map<LocalDate, Long> metrics = countByNextRevision(cards);
		fixFirstDayStats(from, metrics);
		
		return metrics;
	}
	
	private void fixFirstDayStats(LocalDate firstDay, Map<LocalDate, Long> metrics) {
		TreeSet<Entry<LocalDate, Long>> entries = new TreeSet<>(Comparator.comparing(Entry::getKey));
		entries.addAll(metrics.entrySet());
		
		LinkedList<Entry<LocalDate, Long>> preFirstDay = new LinkedList<>();
		for (Entry<LocalDate, Long> entry : entries) {
			if(entry.getKey().isBefore(firstDay)) {
				metrics.remove(entry.getKey());
				preFirstDay.add(entry);
			} else {
				computeFirstDay(firstDay, preFirstDay, metrics);
				break;
			}
		}
	}
	
	private void computeFirstDay(LocalDate firstDay, LinkedList<Entry<LocalDate, Long>> preFirstDate, Map<LocalDate, Long> metrics) {
		metrics.compute(firstDay, (key, value) -> {
			long count = count(preFirstDate);
			if(value == null) {
				return count;
			} else {
				return value += count;
			}
		});
	}
	
	private long count(List<Entry<LocalDate, Long>> preFirstDate) {
		return preFirstDate.stream()
			.map(i -> i.getValue())
			.reduce(Long.valueOf(0), (accumulator, actual) -> accumulator + actual);
	}
	
	private Map<LocalDate, Long> countByNextRevision(Stream<Card> data) {
		return data.filter(i -> i.getMetric().getNextRevision() != null)
			.collect(Collectors.groupingBy(i -> i.getMetric().getNextRevision()))
			.entrySet()
			.stream()
			.map(i -> Pair.create(i.getKey(), i.getValue().size()))
			.collect(Collectors.toMap(i -> (LocalDate) i.getKey(), i -> (Long) i.getValue().longValue()));
	}

	@Override
	public Map<LocalDate, Long> countAddedCards(Long accountId, LocalDate from, LocalDate to) {
		return cardRepo.streamWithMetricByAccount(accountId)
			.filter(i -> i.getCreateTimestamp().isAfter(from.atStartOfDay()))
			.filter(i -> i.getCreateTimestamp().isBefore(to.plusDays(1).atStartOfDay()))
			.map(i -> Pair.create(i.getCreateTimestamp(), i.getId()))
			.collect(Collectors.groupingBy(i -> i.getKey().toLocalDate()))
			.entrySet().stream()
			.collect(Collectors.toMap(i -> i.getKey(), i -> Integer.valueOf(i.getValue().size()).longValue()));
	}
}
