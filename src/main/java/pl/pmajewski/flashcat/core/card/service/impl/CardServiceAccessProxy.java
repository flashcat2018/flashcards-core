package pl.pmajewski.flashcat.core.card.service.impl;

import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.auth.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.core.card.pojo.CardPatchPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.card.service.CardService;
import pl.pmajewski.flashcat.core.tally.exception.TallyNotFoundException;
import pl.pmajewski.flashcat.core.tally.model.Tally;
import pl.pmajewski.flashcat.core.tally.model.Tally.Access;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;

@Component
public class CardServiceAccessProxy implements CardService {

	private CardRepository cardRepo;
	private TallyRepository tallyRepo;
	private CardService cardService;
	private Auth auth;

	
	public CardServiceAccessProxy(CardRepository cardRepo, TallyRepository tallyRepo, CardServiceImpl cardService, Auth auth) {
		this.cardRepo = cardRepo;
		this.tallyRepo = tallyRepo;
		this.cardService = cardService;
		this.auth = auth;
	}

	@Override
	public CardPojo getCardById(Long cardId) {
		cardRepo.findById(cardId).ifPresent(card -> {
			if(card.getTally().getAccess().equals(Access.PRIVATE)
					&& !auth.getUserId().equals(card.getTally().getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return cardService.getCardById(cardId);
	}

	@Override
	public CardPojo addCard(Long tallyId, CardPojo cardPojo) {
		tallyRepo.findById(tallyId).ifPresent(tally -> {
			if(!auth.getUserId().equals(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return cardService.addCard(tallyId, cardPojo);
	}

	@Override
	public CardPojo updateCard(CardPojo cardPojo) {
		cardRepo.findById(cardPojo.getId()).ifPresent(card -> {
			if(!auth.getUserId().equals(card.getTally().getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return cardService.updateCard(cardPojo);
	}

	@Override
	public void deleteCard(Long cardId) {
		cardRepo.findById(cardId).ifPresent(card -> {
			if(!auth.getUserId().equals(card.getTally().getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		cardService.deleteCard(cardId);
	}

	@Override
	public void patchCard(Long cardId, CardPatchPojo body) {
		cardRepo.findById(cardId).ifPresent(card -> {
			if(!auth.getUserId().equals(card.getTally().getAccountId())) {
				throw new UnauthorizedAccessException();
			}
			
			if(body.getTallyId() != null) {
				Tally tally = tallyRepo.findById(body.getTallyId())
					.orElseThrow(() -> new TallyNotFoundException("Tally with id="+body.getTallyId()+" not found"));
				
				if(!tally.getAccountId().equals(auth.getUserId())) {
					throw new UnauthorizedAccessException();
				}
			}
		});
		
		cardService.patchCard(cardId, body);
	}
}
