package pl.pmajewski.flashcat.core.card.pojo;

import lombok.Data;

@Data
public class AnswerPojo {
	
	private Long id;
	private Long sequenceId;
	private String contentType;
	private String content;
}
