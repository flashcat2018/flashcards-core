package pl.pmajewski.flashcat.core.card.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.model.Metric;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.learn.engine.LearnAlgorithm;
import pl.pmajewski.flashcat.core.learn.engine.dto.CardMetric;
import pl.pmajewski.flashcat.core.learn.exception.LearnException;

@Component
@Slf4j
public class CardMetricInitilializer {


	protected PlatformTransactionManager txManager;

	private CardRepository cardRepo;
	private LearnAlgorithm learnAlgorithm;
	
	public CardMetricInitilializer(CardRepository cardRepo, LearnAlgorithm learnAlgorithm) {
		this.cardRepo = cardRepo;
		this.learnAlgorithm = learnAlgorithm;
	}

	@PostConstruct
	public void init() throws LearnException {
		TransactionTemplate tmpl = new TransactionTemplate(txManager);
		tmpl.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				try {
					process();
				} catch (LearnException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void process() throws LearnException {
		Iterable<Card> findAll = cardRepo.findAll();
		
		for (Card card : findAll) {
			Metric cardMetric = card.getMetric();
			if(cardMetric == null) {
				log.info("Initialize metrics for card="+card);
				CardMetric metric = learnAlgorithm.getMetric(card);
				
				cardMetric = new Metric(card);
				cardMetric.setFirstRevision(metric.firstRevision);
				cardMetric.setLastRevision(metric.lastRevision);
				cardMetric.setNextRevision(metric.nextRevision);
				
				card.setMetric(cardMetric);
				
				cardRepo.save(card);
			} else if(cardMetric.getNextRevision() == null) {
				CardMetric metric = learnAlgorithm.getMetric(card);
				card.getMetric().setNextRevision(metric.nextRevision);
				cardRepo.save(card);
			}
		}
	}
	
	@Autowired
	@Qualifier("transactionManager")
	public void setTxManager(PlatformTransactionManager txManager) {
		this.txManager = txManager;
	}
}
