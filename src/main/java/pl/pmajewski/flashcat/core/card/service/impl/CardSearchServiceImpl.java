package pl.pmajewski.flashcat.core.card.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardDescPojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.card.service.CardSearchService;
import pl.pmajewski.flashcat.core.card.utils.SearchUtils;
import pl.pmajewski.flashcat.core.dto.SearchDTO;

@Service
@Transactional
public class CardSearchServiceImpl implements CardSearchService {

	private CardRepository cardRepository;
	
	public CardSearchServiceImpl(pl.pmajewski.flashcat.core.card.repository.CardRepository cardRepository) {
		this.cardRepository = cardRepository;
	}

	@Override
	public SearchDTO<CardDescPojo> searchQuestions(Long tallyId, String query, Integer limit, Integer offset) {
		List<String> formatedQuery = Arrays.asList(query.split(" "));
		List<CardDescPojo> collect = new ArrayList<>();
		Long count = null;
		
		if(limit == 0) {
 			collect = cardRepository.searchQuestions(tallyId, formatedQuery, PageRequest.of(0, Integer.MAX_VALUE))
 					.stream()
 					.map(Card::getDescPojo)
 					.collect(Collectors.toList());
 			 count = ((Integer) collect.size()).longValue();
 		} else {
 			count = cardRepository.countQuestions(tallyId, formatedQuery);
 			if(count > 0) {
 				collect = cardRepository.searchQuestions(tallyId, formatedQuery, 
 							PageRequest.of(SearchUtils.toPage(offset, limit), limit))
 						.stream()
 						.map(Card::getDescPojo)
 						.collect(Collectors.toList());
 			}
 		}
		
		return new SearchDTO<>(collect, count, offset, limit);
	}
	
}
