package pl.pmajewski.flashcat.core.card.pojo;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CardsMetricDTO {

	private LocalDate date;
	private Long quantity;
	
}
