package pl.pmajewski.flashcat.core.card.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.hibernate.annotations.DiscriminatorOptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "part_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorOptions(force = true)
@Table(name = "card_parts", schema = "core")
@Data
@EqualsAndHashCode(of = {"id", "sequenceId"})
@ToString(exclude = "card")
public abstract class CardPart {
	
	public enum ContentType { TEXT, IMAGE }
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	@Column(name = "sequence_id")
	protected Long sequenceId;
	
	@Column(name = "content_type", nullable = false)
	@Enumerated(EnumType.STRING)
	protected ContentType contentType;
	
	@Column(columnDefinition = "TEXT")
	protected String content;
	
	@Column(name = "create_timestamp")
	protected LocalDateTime createTimestamp;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "card_id")
	protected Card card;
	
	@PrePersist
	private void prePersist() {
		this.createTimestamp = LocalDateTime.now();
	}
}
