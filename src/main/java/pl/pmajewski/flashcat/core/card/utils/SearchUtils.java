package pl.pmajewski.flashcat.core.card.utils;

public class SearchUtils {

	/**
	 * Converter to page number.
	 * Zero-based page indexing 
	 */
	public static int toPage(int offset, int limit) {
		if(limit == 0) {
			return 0;
		}
		
		return offset / limit;
	}
}
