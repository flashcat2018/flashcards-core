package pl.pmajewski.flashcat.core.card.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pmajewski.flashcat.core.card.pojo.QuestionPojo;

@Entity
@DiscriminatorValue("QUESTION")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QuestionCard extends CardPart {
	
	public QuestionCard(Long id) {
		this.id = id;
	}
	
	public QuestionPojo getPojo() {
		QuestionPojo p = new QuestionPojo();
		
		p.setId(id);
		p.setSequenceId(sequenceId);
		p.setContentType(contentType.toString());
		p.setContent(content);

		return p;
	}
}
