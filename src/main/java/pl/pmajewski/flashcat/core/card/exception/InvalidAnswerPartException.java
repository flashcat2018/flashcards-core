package pl.pmajewski.flashcat.core.card.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InvalidAnswerPartException extends RuntimeException {

	public InvalidAnswerPartException() {
		super();
	}

	public InvalidAnswerPartException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidAnswerPartException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidAnswerPartException(String message) {
		super(message);
	}

	public InvalidAnswerPartException(Throwable cause) {
		super(cause);
	}
}
