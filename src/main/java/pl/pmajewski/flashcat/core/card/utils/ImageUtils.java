package pl.pmajewski.flashcat.core.card.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ImageUtils {
	
	public static String mimeToImageFormat(String mime) {
		String[] splited = mime.split("/");
		return splited[1];
	}
}
