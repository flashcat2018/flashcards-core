package pl.pmajewski.flashcat.core.card.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ImageProcessingException extends RuntimeException {

	public ImageProcessingException(Throwable cause) {
		super(cause);
	}

	public ImageProcessingException(String message) {
		super(message);
	}
}
