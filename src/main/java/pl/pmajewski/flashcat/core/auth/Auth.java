package pl.pmajewski.flashcat.core.auth;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import pl.pmajewski.flashcat.core.config.ContextMgr;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Auth {

	@Autowired
	private HttpServletRequest request;
	
	private Long userId;
	
	@PostConstruct
	private void init() {
		if(request.getHeader("userId") != null) {
			userId = Long.parseLong(request.getHeader("userId"));
		}
	}
	
	public static Long userId() {
		try {
			Auth auth = (Auth) ContextMgr.getContext().getBean("auth");
			Long userId = auth.getUserId();
			return userId;
		} catch(Exception e) {
			return -1l;
		}
	}
	
	public Long getUserId() {
		if(userId == null) {
			return -1l;
		}
		
		return userId;
	}
}
