package pl.pmajewski.flashcat.core.tally.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.dto.SearchDTO;
import pl.pmajewski.flashcat.core.tally.dto.AddTallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyStatsDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyUpdateDTO;
import pl.pmajewski.flashcat.core.tally.service.TallyPersonalService;
import pl.pmajewski.flashcat.core.tally.service.TallySearchService;
import pl.pmajewski.flashcat.core.tally.service.impl.TallyServiceAccessProxy;

@RestController
@CommonsLog
public class TallyController {

	private TallyPersonalService tallyService;
	private TallySearchService tallySearchService;

	public TallyController(TallyServiceAccessProxy tallyService, TallySearchService searchService) {
		this.tallyService = tallyService;
		this.tallySearchService = searchService;
	}
	
	@GetMapping("/accounts/{accountId}/tallies")
	public List<TallyStatsDTO> getTalliesByUser(@PathVariable Long accountId) {
		log.info("["+Auth.userId()+"] List tallies for user: "+accountId);
		return tallyService.list(accountId);
	}
	
	@GetMapping("/tallies/{tallyId}")
	public TallyDTO getTally(@PathVariable Long tallyId) {
		log.info("["+Auth.userId()+"] Get tally id: "+tallyId);
		return tallyService.get(tallyId);
	}
	
	@GetMapping("/tallies")
	public SearchDTO<TallyDTO> listTallies(@RequestParam String query, 
			@RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "0") Integer offset) {
		return tallySearchService.listPublic(query, offset, limit);
	}
	
	@PostMapping("/tallies")
	@ResponseStatus(value = HttpStatus.CREATED)
	public TallyDTO addTally(@RequestBody AddTallyDTO pojo) {
		log.info("["+Auth.userId()+"] Add new tally: "+pojo);
		pojo.setAccountId(Auth.userId());
		return tallyService.addTally(pojo);
	}
	
	@DeleteMapping("/tallies/{id}")
	public void removeTally(@PathVariable Long id) {
		log.info("["+Auth.userId()+"] Remove tallyId: "+id);
		tallyService.removeTally(id);
	}
	
	@DeleteMapping("tallies/{id}/results")
	public void resetResults(@PathVariable Long id) {
		log.info("["+Auth.userId()+"] Reset results for tallyId: "+id);
		tallyService.resetResults(id);
	}
	
	@PatchMapping("/tallies/{id}")
	public void updateTally(@PathVariable("id") Long tallyId, @RequestBody TallyUpdateDTO tally) {
		log.info("["+Auth.userId()+"] Change tallyId "+tallyId+" name: "+tally.getName());
		tallyService.updateTally(tallyId, tally);
	}
}
