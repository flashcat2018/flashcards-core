package pl.pmajewski.flashcat.core.tally.service.exporter.anki;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.ResizableByteArrayOutputStream;

import com.opencsv.CSVWriter;

import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.service.image.Image;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.service.exporter.DataExporter;
import pl.pmajewski.flashcat.core.tally.service.exporter.ExporterDataSource;
import pl.pmajewski.flashcat.core.tally.service.exporter.data.ExporterTallyDTO;
import pl.pmajewski.flashcat.core.tally.service.exporter.exception.ExporterException;

public abstract class AnkiExporter implements DataExporter {

	private ExporterDataSource<ExporterTallyDTO> dataSource;
	private ZipOutputStream zipStream;
	
	private Map<String, CSVWriter> tallies = new HashMap<>();
	
	public AnkiExporter(ExporterDataSource<ExporterTallyDTO> dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public void export(OutputStream destination) throws ExporterException {
		zipStream = new ZipOutputStream(destination);
	}
	
	protected void process(ExporterDataSource<ExporterTallyDTO> dataSource) {
		dataSource.stream()
			.forEach(this::saveTally);
	}
	
	private void saveTally(ExporterTallyDTO tally) {
		tally.getCards()
			.forEach(i -> saveCard(tally.getTallyDTO().getName(), i));
	}
	
	private void saveCard(String tallyName, CardPojo cardPojo) {
	
	}
	
	private void addLine(String tallyName, String[] line) {
		CSVWriter writer = getTallyWriter(tallyName);
		writer.writeNext(line);
	}
	
	private CSVWriter getTallyWriter(String tallyName) {
		tallies.putIfAbsent(tallyName, createWriter(tallyName));
		return tallies.get(tallyName);
	}
	
	private CSVWriter createWriter(String tallyName) {
		return new CSVWriter(new OutputStreamWriter(new ResizableByteArrayOutputStream()));
	}
	
	private String saveImage(Image image) throws IOException {
		ZipEntry entry = new ZipEntry("images/"+RandomStringUtils.randomAlphanumeric(32)+"."+image.getImageFormat());
		zipStream.putNextEntry(entry);
		zipStream.write(image.getByte());
		zipStream.closeEntry();
		return entry.getName();
	}
}
