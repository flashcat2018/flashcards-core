package pl.pmajewski.flashcat.core.tally.service.impl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.service.CardMetricsService;
import pl.pmajewski.flashcat.core.learn.engine.LearnEngine;
import pl.pmajewski.flashcat.core.tally.dto.AddTallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyStatsDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyUpdateDTO;
import pl.pmajewski.flashcat.core.tally.exception.TallyNotFoundException;
import pl.pmajewski.flashcat.core.tally.model.Tally;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;
import pl.pmajewski.flashcat.core.tally.service.TallyPersonalService;
import pl.pmajewski.flashcat.core.tally.utils.TallyUtils;

@Service
@Transactional
public class TallyServiceImpl implements TallyPersonalService {
	
	private TallyRepository tallyRepository;
	private CardMetricsService cardService;
	private LearnEngine learnEngine;
	
	public TallyServiceImpl(TallyRepository tallyRepository, CardMetricsService cardService, LearnEngine learnAlgorithm) {
		this.tallyRepository = tallyRepository;
		this.learnEngine = learnAlgorithm;
		this.cardService = cardService;
	}

	@Override
	public TallyDTO get(Long tallyId) {
		Tally tally = tallyRepository.findById(tallyId)
				.orElseThrow(() -> new TallyNotFoundException("Tally with id: "+tallyId+" not found."));
		
		return tally.getPojo();
	}
	
	@Override
	public List<TallyStatsDTO> list(Long accountId) {
		return tallyRepository.find(accountId)
				.map(Tally::getPojo)
				.map(TallyUtils::mapToStats)
				.map(i -> {
					i.setNumberOfCards(cardService.countAll(i.getId()));
					i.setNumberOfCardsToRepeat(cardService.tallyRepetitions(i.getId()));
					i.setNumberOfNovelties(cardService.tallyNovelties(i.getId()));
					return i;
				})
				.sorted(Comparator.comparing(TallyStatsDTO::getNumberOfCardsToRepeat)
						.thenComparing(TallyStatsDTO::getNumberOfNovelties)
						.thenComparing(TallyStatsDTO::getNumberOfCards).reversed())
				.collect(Collectors.toList());
	}

	@Override
	public TallyDTO addTally(AddTallyDTO dto) {
		Tally tally = Tally.of(dto);
		tallyRepository.save(tally);
		return tally.getPojo();
	}

	@Override
	public void removeTally(Long tallyId) {
		tallyRepository.deleteById(tallyId);
	}

	@Override
	public void resetResults(Long tallyId) {
		learnEngine.resetTally(tallyId);;
	}

	@Override
	public void updateTally(Long tallyId, TallyUpdateDTO body) {
		tallyRepository.findById(tallyId)
			.ifPresent(i -> i.update(body));
	}
}
