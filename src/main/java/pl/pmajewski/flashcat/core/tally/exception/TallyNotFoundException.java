package pl.pmajewski.flashcat.core.tally.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TallyNotFoundException extends RuntimeException {

	public TallyNotFoundException() {
		super();
	}

	public TallyNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public TallyNotFoundException(String message) {
		super(message);
	}

	public TallyNotFoundException(Throwable cause) {
		super(cause);
	}
}
