package pl.pmajewski.flashcat.core.tally.service.exporter.xlsx;

import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.WorkbookUtil;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.exception.ImageProcessingException;
import pl.pmajewski.flashcat.core.card.model.CardPart;
import pl.pmajewski.flashcat.core.card.pojo.AnswerPojo;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.pojo.QuestionPojo;
import pl.pmajewski.flashcat.core.card.service.image.ImageCardAdapter;
import pl.pmajewski.flashcat.core.tally.service.exporter.ExporterDataSource;
import pl.pmajewski.flashcat.core.tally.service.exporter.data.ExporterTallyDTO;
import pl.pmajewski.flashcat.core.tally.service.exporter.exception.ExporterException;

@Slf4j
public class XlsxTallyExporter extends XlsxExporter<ExporterTallyDTO> {
	
	private static final int QUESTION_COLUMN_WIDTH = 256*40;
	private static final int COLUMN_WIDTH = 256*45;
	private static final float ROW_HEIGHT = 280;
	
	public XlsxTallyExporter(ExporterDataSource<ExporterTallyDTO> dataSource) {
		super(dataSource);
	}

	@Override
	protected void write(Stream<ExporterTallyDTO> data) throws ExporterException {
		data.forEach(i -> writeTally(i));
	}
	
	private void writeTally(ExporterTallyDTO tally) {
		Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName(tally.getTallyDTO().getName()));
		initColumnSize(sheet);
		
		tally.getCards().forEach(i -> {
			Row row = sheet.createRow(sheet.getLastRowNum()+1);
			writeCard(sheet, row, i);
		});
	}
	
	private void initColumnSize(Sheet sheet) {
		sheet.setColumnWidth(0, QUESTION_COLUMN_WIDTH);
		for(int i=1; i < 12; i++) {
			sheet.setColumnWidth(i, COLUMN_WIDTH);
		}		
	}
	
	private void writeCard(Sheet sheet, Row row, CardPojo card) {
		writeRow(sheet, row, card);
	}
	
	private void writeRow(Sheet sheet, Row row, CardPojo cardPojo) {
		writeQuestion(row, cardPojo.getQuestion());
		writeAnswers(sheet, row, cardPojo.getAnswers());
	}
	
	private void writeQuestion(Row row, QuestionPojo question) {
		Cell cell = row.createCell(0);
		cell.getRow().setHeightInPoints(ROW_HEIGHT);
		cell.getCellStyle().setWrapText(true);
		cell.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		cell.setCellValue(question.getContent());
	}

	private void writeAnswers(Sheet sheet, Row row, AnswerPojo[] answers) {
		for (AnswerPojo answer : answers) {
			Cell cell = row.createCell(row.getLastCellNum());

			switch (CardPart.ContentType.valueOf(answer.getContentType())) {
			case TEXT:
				addText(cell, answer);
				break;
			case IMAGE:
				try {
					addImage(sheet, cell, ImageCardAdapter.from(answer));
				} catch(ImageProcessingException e) {
					log.error("["+Auth.userId()+"] Cannot extract image for answerId="+answer.getId()+". ErrorMessage="+e.getMessage());
				}
				break;
			default:
				throw new ExporterException("Unsupported answer content type.");
			}
		}
	}
	
	private void addText(Cell cell, AnswerPojo answer) {
		cell.getCellStyle().setWrapText(true);
		cell.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		cell.setCellValue(answer.getContent());
	}
}
