package pl.pmajewski.flashcat.core.tally.dto;

import lombok.Data;

@Data
public class TallyUpdateDTO {

	private String name;
	private String description;
	private String access;
	
}
