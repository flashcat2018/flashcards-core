package pl.pmajewski.flashcat.core.tally.service.exporter;

import java.io.OutputStream;

import pl.pmajewski.flashcat.core.tally.service.exporter.exception.ExporterException;

public interface DataExporter {
	
	void export(OutputStream destination) throws ExporterException;
}
