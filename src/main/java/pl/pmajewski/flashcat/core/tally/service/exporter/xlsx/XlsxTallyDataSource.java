package pl.pmajewski.flashcat.core.tally.service.exporter.xlsx;

import java.util.stream.Stream;

import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;
import pl.pmajewski.flashcat.core.tally.service.exporter.ExporterDataSource;
import pl.pmajewski.flashcat.core.tally.service.exporter.data.ExporterTallyDTO;

public class XlsxTallyDataSource implements ExporterDataSource<ExporterTallyDTO> {

	private Long accountId;
	private TallyRepository tallyRepo;
	private CardRepository cardRepo;
	
	private XlsxTallyDataSource(Builder builder) {
		this.accountId = builder.accountId;
		this.tallyRepo = builder.tallyRepository;
		this.cardRepo = builder.cardRepository;
	}
	
	@Override
	public Stream<ExporterTallyDTO> stream() {
		return tallyRepo.find(accountId)
			.map(i -> new ExporterTallyDTO(i.getPojo(), cardRepo));
		
	}
	
	public static Builder builder() {
		return new Builder();
	}
	
	public static class Builder {
		
		private Long accountId;
		private TallyRepository tallyRepository;
		private CardRepository cardRepository;
		
		public Builder accountId(Long accountId) {
			this.accountId = accountId;
			return this;
		}
		
		public Builder tallyRepository(TallyRepository tallyRepository) {
			this.tallyRepository = tallyRepository;
			return this;
		}
		
		public Builder cardRepository(CardRepository cardRepository) {
			this.cardRepository = cardRepository;
			return this;
		}
		
		public XlsxTallyDataSource build() {
			return new XlsxTallyDataSource(this);
		}
	}
}
