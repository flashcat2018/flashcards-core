package pl.pmajewski.flashcat.core.tally.service.exporter.exception;

public class ExporterException extends RuntimeException {

	public ExporterException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExporterException(Throwable cause) {
		super(cause);
	}

	public ExporterException(String message) {
		super(message);
	}
}
