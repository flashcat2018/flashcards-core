package pl.pmajewski.flashcat.core.tally.utils;

import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyStatsDTO;

public class TallyUtils {

	public static TallyStatsDTO mapToStats(TallyDTO dto) {
		TallyStatsDTO out = new TallyStatsDTO();
		
		out.setId(dto.getId());
		out.setName(dto.getName());
		out.setDescription(dto.getDescription());
		out.setCreateTimestamp(dto.getCreateTimestamp());
		out.setAuthorId(dto.getAuthorId());
		out.setAccess(dto.getAccess());
		
		return out;
	}
}
