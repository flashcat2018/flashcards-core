package pl.pmajewski.flashcat.core.tally.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import pl.pmajewski.flashcat.core.dto.SearchDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.model.Tally;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;
import pl.pmajewski.flashcat.core.tally.service.TallySearchService;

@Service
public class TallySearchServiceImpl implements TallySearchService {

	private TallyRepository tallyRepository;
	
	public TallySearchServiceImpl(TallyRepository tallyRepository) {
		this.tallyRepository = tallyRepository;
	}

	@Override
	public SearchDTO<TallyDTO> listPublic(String query, Integer offset, Integer limit) {
		Long count = tallyRepository.countPublic("%"+query+"%");
		List<TallyDTO> data = tallyRepository.listPublic("%"+query+"%", PageRequest.of(offset, limit))
				.stream()
				.map(Tally::getPojo)
				.collect(Collectors.toList());
		return new SearchDTO<>(data, count, offset, limit);
	}
	
}
