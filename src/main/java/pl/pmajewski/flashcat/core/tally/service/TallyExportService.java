package pl.pmajewski.flashcat.core.tally.service;

import org.springframework.core.io.ByteArrayResource;

public interface TallyExportService {

	ByteArrayResource generateXlsx(Long accountId);
	
	ByteArrayResource generateAnki(Long accountId);
}
