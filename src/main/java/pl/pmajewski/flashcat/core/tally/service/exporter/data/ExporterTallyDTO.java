package pl.pmajewski.flashcat.core.tally.service.exporter.data;

import java.util.stream.Stream;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.pojo.CardPojo;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;

public class ExporterTallyDTO {

	private TallyDTO tallyDto;
	private CardRepository cardRepository;
	
	public ExporterTallyDTO(TallyDTO tallyDto, CardRepository cardRepository) {
		this.tallyDto = tallyDto;
		this.cardRepository = cardRepository;
	}
	
	public TallyDTO getTallyDTO() {
		return tallyDto;
	}
	
	public Stream<CardPojo> getCards() {
		return cardRepository.streamWithParts(tallyDto.getId())
				.map(Card::getPojo);
	}
}
