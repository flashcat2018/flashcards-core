package pl.pmajewski.flashcat.core.tally.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.auth.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.core.tally.dto.AddTallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyStatsDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyUpdateDTO;
import pl.pmajewski.flashcat.core.tally.model.Tally.Access;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;
import pl.pmajewski.flashcat.core.tally.service.TallyPersonalService;

@Component
public class TallyServiceAccessProxy implements TallyPersonalService {

	private TallyPersonalService tallyService;
	private TallyRepository tallyRepo;
	private Auth auth;
	
	public TallyServiceAccessProxy(TallyServiceImpl tallyService, TallyRepository tallyRepo, Auth auth) {
		this.tallyService = tallyService;
		this.tallyRepo = tallyRepo;
		this.auth = auth;
	}
	
	@Override
	public TallyDTO get(Long tallyId) {
		tallyRepo.findById(tallyId).ifPresent(tally -> {
			if(tally.getAccess().equals(Access.PRIVATE) 
				&& !hasAccess(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		return tallyService.get(tallyId);
	}

	@Override
	public List<TallyStatsDTO> list(Long accountId) {
		if(!hasAccess(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return tallyService.list(accountId);
	}

	@Override
	public TallyDTO addTally(AddTallyDTO dto) {
		if(!auth.getUserId().equals(dto.getAccountId())) {
			throw new UnauthorizedAccessException();
		}
		
		return tallyService.addTally(dto);
	}

	@Override
	public void removeTally(Long tallyId) {
		tallyRepo.findById(tallyId).ifPresent(tally -> {
			if(!hasAccess(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		tallyService.removeTally(tallyId);
	}

	@Override
	public void resetResults(Long tallyId) {
		tallyRepo.findById(tallyId).ifPresent(tally -> {
			if(!hasAccess(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		tallyService.resetResults(tallyId);
	}

	@Override
	public void updateTally(Long tallyId, TallyUpdateDTO body) {
		tallyRepo.findById(tallyId).ifPresent(tally -> {
			if(!hasAccess(tally.getAccountId())) {
				throw new UnauthorizedAccessException();
			}
		});
		
		tallyService.updateTally(tallyId, body);
	}
	
	private boolean hasAccess(Long accountId) {
		return auth.getUserId().equals(accountId);
	}
}