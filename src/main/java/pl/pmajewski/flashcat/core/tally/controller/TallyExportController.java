package pl.pmajewski.flashcat.core.tally.controller;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.pmajewski.flashcat.core.exception.InvalidInputData;
import pl.pmajewski.flashcat.core.tally.service.TallyExportService;
import pl.pmajewski.flashcat.core.tally.service.impl.TallyExportAccessProxy;

@RestController
public class TallyExportController {

	private TallyExportService exportService;
	
	public TallyExportController(TallyExportAccessProxy tallyExport) {
		this.exportService = tallyExport;
	}
	
	@GetMapping(value = "/export/tallies")
	public ResponseEntity<Resource> exportTally(@RequestParam String format, @RequestParam Long accountId) {
		if(format.equalsIgnoreCase("xlsx")) {
			ByteArrayResource body = exportService.generateXlsx(accountId);
			return ResponseEntity.ok()
				.contentLength(body.contentLength())
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
				.header("Content-disposition", "attachment; filename=backup.xlsx")
				.body(body);
		} 
		
		throw new InvalidInputData();
	}
}
