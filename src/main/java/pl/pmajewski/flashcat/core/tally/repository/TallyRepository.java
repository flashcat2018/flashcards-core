package pl.pmajewski.flashcat.core.tally.repository;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.pmajewski.flashcat.core.tally.model.Tally;

public interface TallyRepository extends CrudRepository<Tally, Long> {
	
	@Query("select t from Tally t where accountId=:accountId")
	Stream<Tally> find(@Param("accountId") Long authorId);
	
	@Query("select t from Tally t where t.access='PUBLIC' and upper(t.name) like upper(:query) order by t.lastUpdateTimestamp desc")
	List<Tally> listPublic(@Param("query") String query, Pageable pageable);
	
	@Query("select count(t) from Tally t where t.access='PUBLIC' and upper(t.name) like upper(:query)")
	Long countPublic(@Param("query") String query);
}
