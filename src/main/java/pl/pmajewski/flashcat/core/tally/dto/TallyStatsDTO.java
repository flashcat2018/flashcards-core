package pl.pmajewski.flashcat.core.tally.dto;

import lombok.Data;

@Data
public class TallyStatsDTO extends TallyDTO {
	
	private Long numberOfCards;
	private Long numberOfCardsToRepeat;
	private Long numberOfNovelties;
}
