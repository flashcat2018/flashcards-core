package pl.pmajewski.flashcat.core.tally.service.exporter.xlsx;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.service.image.Image;
import pl.pmajewski.flashcat.core.tally.service.exporter.DataExporter;
import pl.pmajewski.flashcat.core.tally.service.exporter.ExporterDataSource;
import pl.pmajewski.flashcat.core.tally.service.exporter.exception.ExporterException;

@Slf4j
public abstract class XlsxExporter<T> implements DataExporter, Closeable {

	private ExporterDataSource<T> dataSource;
	protected Workbook workbook = new XSSFWorkbook();

	public XlsxExporter(ExporterDataSource<T> dataSource) {
		this.dataSource = dataSource;
	}

	protected abstract void write(Stream<T> data) throws ExporterException;

	@Override
	public void export(OutputStream destination) throws ExporterException {
		try {
			write(dataSource.stream());
			workbook.write(destination);
			workbook.close();
		} catch (ExporterException e) {
			throw e;
		} catch (Exception e) {
			log.error("["+Auth.userId()+"]", e);
			throw new ExporterException(e);
		}
	}
	
	@Override
	public void close() throws IOException {
		workbook.close();
		dataSource = null;
	}
	
	protected void addImage(Sheet sheet, Cell cell, Image image) {
		int pictureIdx = workbook.addPicture(image.getByte(), getFormat(image.getImageFormat()));
		CreationHelper helper = workbook.getCreationHelper();
		Drawing drawing = sheet.createDrawingPatriarch();
		ClientAnchor anchor = helper.createClientAnchor();
		anchor.setCol1(cell.getColumnIndex()); 
		anchor.setRow1(cell.getRowIndex()); 
		Picture picture = drawing.createPicture(anchor, pictureIdx);
		resizeImage(picture, image);
	}
	
	private void resizeImage(Picture picture, Image img) {
		double ratio = (1.0 * img.getWidth()) / (1.0d * img.getHeight());
		
		if(ratio >= 1d) {
			picture.resize(1d, 1d/ratio);
		} else {
			picture.resize(1d, 1d);
		}
	}
	
	private int getFormat(String imgFormat) {
		if("jpeg".equalsIgnoreCase(imgFormat)) {
			return Workbook.PICTURE_TYPE_JPEG;
		}
		
		if("png".equalsIgnoreCase(imgFormat)) {
			return Workbook.PICTURE_TYPE_PNG;
		}
		
		throw new ExporterException("Unsupported image format -> "+imgFormat);
	}
}
