package pl.pmajewski.flashcat.core.tally.dto;

import lombok.Data;

@Data
public class AddTallyDTO {
	
	private Long accountId;
	private String name;
	private String description;
	private String access;
}
