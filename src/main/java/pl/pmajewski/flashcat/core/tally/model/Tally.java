package pl.pmajewski.flashcat.core.tally.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.tally.dto.AddTallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyUpdateDTO;

@Entity
@Table(name = "tallies", schema = "core")
@Data
@EqualsAndHashCode(of = "id")
public class Tally {
	
	public enum Access {PRIVATE, PUBLIC}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column(name = "create_timestamp")
	private LocalDateTime createTimestamp;
	
	@Column(name = "last_update_timestamp")
	private LocalDateTime lastUpdateTimestamp;
	
	@Column(name = "account_id")
	private Long accountId;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "tally", orphanRemoval = true)
	private Set<Card> cards;
	
	@Column(name = "access")
	@Enumerated(EnumType.STRING)
	private Access access;
	
	@PrePersist
	private void prePersist() {
		this.createTimestamp = LocalDateTime.now();
		this.lastUpdateTimestamp = LocalDateTime.now();
	}
	
	@PreUpdate
	private void preUpdate() {
		this.lastUpdateTimestamp = LocalDateTime.now();
	}
	
	public Tally() { }
	
	public Tally(Long id) {
		this.id = id;
	}
	
	public TallyDTO getPojo() {
		TallyDTO p = new TallyDTO();
		
		p.setId(id);
		p.setName(name);
		p.setDescription(description);
		p.setCreateTimestamp(createTimestamp);
		p.setAuthorId(accountId);
		p.setAccess(access.toString());
		
		return p;
	}
	
	public void update(TallyUpdateDTO body) {
		if(body.getName() != null && !body.getName().isEmpty()) {
			name = body.getName();
		}
		
		if(body.getAccess() != null && !body.getAccess().isEmpty()) {
			access = Tally.Access.valueOf(body.getAccess());
		}
		
		description = body.getDescription();
	}
	
	public static Tally of(AddTallyDTO dto) {
		Tally tally = new Tally();
		tally.setName(dto.getName());
		tally.setDescription(dto.getDescription());
		tally.setAccountId(dto.getAccountId());
		
		if(dto.getAccess() != null && !dto.getAccess().isEmpty()) {
			tally.setAccess(Tally.Access.valueOf(dto.getAccess()));
		} else {
			tally.setAccess(Tally.Access.PRIVATE);
		}
		
		return tally;
	}

}
