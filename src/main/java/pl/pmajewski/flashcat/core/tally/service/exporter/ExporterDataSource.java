package pl.pmajewski.flashcat.core.tally.service.exporter;

import java.util.stream.Stream;

public interface ExporterDataSource<T> {

	Stream<T> stream();
	
}
