package pl.pmajewski.flashcat.core.tally.service.impl;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.auth.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.core.tally.service.TallyExportService;

@Service
public class TallyExportAccessProxy implements TallyExportService {

	private TallyExportService exportService;
	
	public TallyExportAccessProxy(TallyExportServiceImpl exportService) { 
		this.exportService = exportService;
	} 
	
	@Override
	public ByteArrayResource generateXlsx(Long accountId) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return exportService.generateXlsx(accountId);
	}

	@Override
	public ByteArrayResource generateAnki(Long accountId) {
		if(!Auth.userId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		return exportService.generateXlsx(accountId);
	}
}
