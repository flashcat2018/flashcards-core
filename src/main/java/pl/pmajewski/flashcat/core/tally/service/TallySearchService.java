package pl.pmajewski.flashcat.core.tally.service;

import pl.pmajewski.flashcat.core.dto.SearchDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;

public interface TallySearchService {

	SearchDTO<TallyDTO> listPublic(String query, Integer offset, Integer limit);
}
