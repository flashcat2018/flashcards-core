package pl.pmajewski.flashcat.core.tally.service;

import java.util.List;

import pl.pmajewski.flashcat.core.tally.dto.AddTallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyStatsDTO;
import pl.pmajewski.flashcat.core.tally.dto.TallyUpdateDTO;

public interface TallyPersonalService {
	
	TallyDTO get(Long tallyId);
	
	List<TallyStatsDTO> list(Long accountId);
	
	TallyDTO addTally(AddTallyDTO pojo);

	void removeTally(Long tallyId);
	
	void resetResults(Long tallyId);
	
	void updateTally(Long tallyId, TallyUpdateDTO tally);
}
