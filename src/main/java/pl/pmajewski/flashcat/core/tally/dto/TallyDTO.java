package pl.pmajewski.flashcat.core.tally.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TallyDTO {
	
	private Long id;
	private String name;
	private String description;
	private LocalDateTime createTimestamp;
	private Long authorId;
	private String access;
}
