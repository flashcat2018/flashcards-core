package pl.pmajewski.flashcat.core.tally.service.impl;

import java.io.IOException;

import javax.transaction.Transactional;

import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResizableByteArrayOutputStream;

import lombok.extern.slf4j.Slf4j;
import pl.pmajewski.flashcat.core.auth.Auth;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;
import pl.pmajewski.flashcat.core.tally.repository.TallyRepository;
import pl.pmajewski.flashcat.core.tally.service.TallyExportService;
import pl.pmajewski.flashcat.core.tally.service.exporter.ExporterDataSource;
import pl.pmajewski.flashcat.core.tally.service.exporter.exception.ExporterException;
import pl.pmajewski.flashcat.core.tally.service.exporter.xlsx.XlsxExporter;
import pl.pmajewski.flashcat.core.tally.service.exporter.xlsx.XlsxTallyDataSource;
import pl.pmajewski.flashcat.core.tally.service.exporter.xlsx.XlsxTallyExporter;

@Service
@Transactional
@Primary
@Slf4j
public class TallyExportServiceImpl implements TallyExportService {

	private TallyRepository tallyRepository;
	private CardRepository cardRepository;
	
	public TallyExportServiceImpl(TallyRepository tallyRepository, CardRepository cardRepository) {
		this.tallyRepository = tallyRepository;
		this.cardRepository = cardRepository;
	}

	@Override
	public ByteArrayResource generateXlsx(Long accountId) {
		ExporterDataSource dataSource = XlsxTallyDataSource.builder()
			.accountId(accountId)
			.cardRepository(cardRepository)
			.tallyRepository(tallyRepository)
			.build();
		
		try(ResizableByteArrayOutputStream os = new ResizableByteArrayOutputStream(); 
				XlsxExporter exporter = new XlsxTallyExporter(dataSource)) {
			exporter.export(os);
			exporter.close();
			
			return new ByteArrayResource(os.toByteArray());
		} catch (IOException e) {
			log.error("["+Auth.userId()+"] Xlsx generation fail", e);
			throw new ExporterException("Xlsx generation fail");
		}
	}

	@Override
	public ByteArrayResource generateAnki(Long accountId) {
		
		
		return null;
	}
}
