package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config.FC1Properties;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;

public class FC1SelectorTestData {

	public static Stream<FC1CardRating> novelties() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cNovelty(1l));
		r.add(cNovelty(2l));
		r.add(cNovelty(3l));
		r.add(cNovelty(4l));
		r.add(cNovelty(5l));
		
		return r.stream();
	}
	
	public static Stream<FC1CardRating> remaind() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cRemaind(6l));
		r.add(cRemaind(7l));
		r.add(cRemaind(8l));
		r.add(cRemaind(9l));
		
		return r.stream();
	}
	
	public static Stream<FC1CardRating> rescue() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cRescue(10l));
		r.add(cRescue(11l));
		r.add(cRescue(12l));
		r.add(cRescue(13l));
		
		return r.stream();
	}
	
	public static Stream<FC1CardRating> learn() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cLearn(14l));
		r.add(cLearn(15l));
		r.add(cLearn(16l));
		r.add(cLearn(17l));

		return r.stream();
	}
	
	public static Stream<FC1CardRating> progressiveRemaind() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cRescue(18l));
		r.add(cLearn(19l));
		r.add(cRemaind(20l));
		r.add(cNovelty(21l));
		
		return r.stream();
	}
	
	public static Stream<FC1CardRating> progressiveRescue() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cLearn(22l));
		r.add(cRescue(23l));
		r.add(cLearn(24l));
		r.add(cNovelty(25l));
		r.add(cNovelty(26l));
		
		return r.stream();
	}
	
	public static Stream<FC1CardRating> progressiveLearn() {
		List<FC1CardRating> r = new ArrayList<>();
		r.add(cLearn(29l));
		r.add(cNovelty(30l));
		r.add(cNovelty(31l));
		
		return r.stream();
	}
	
	private static FC1CardRating cNovelty(Long cardId) {
		return new FC1CardRating(new Card(cardId));
	}
	
	private static FC1CardRating cRemaind(Long cardId) {
		return FC1CardRating.builder()
				.cardId(cardId)
				.lastRevision(LocalDate.now().minusDays(1))
				.nextRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1))
				.easinessFactor(FC1Properties.EF_INIT)
				.interval(FC1Properties.INTERVAL_INIT)
				.build();
	}
	
	private static FC1CardRating cRescue(Long cardId) {
		return FC1CardRating.builder()
				.cardId(cardId)
				.lastRevision(LocalDate.now())
				.nextRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1))
				.easinessFactor(FC1Properties.EF_INIT)
				.interval(FC1Properties.INTERVAL_INIT)
				.repetition(1)
				.build();
	}
	
	private static FC1CardRating cLearn(Long cardId) {
		return FC1CardRating.builder()
			.cardId(cardId)
			.lastRevision(LocalDate.now())
			.nextRevision(LocalDate.now())
			.firstRevision(LocalDate.now().minusDays(1))
			.easinessFactor(FC1Properties.EF_INIT)
			.interval(FC1Properties.INTERVAL_INIT)
			.repetition(2)
			.build();
	}
}
