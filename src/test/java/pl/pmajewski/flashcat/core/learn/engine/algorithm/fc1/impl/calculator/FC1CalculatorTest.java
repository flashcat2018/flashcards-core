package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.calculator;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.apache.commons.lang.SerializationUtils;
import org.junit.Test;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.config.FC1Properties;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.model.FC1CardRating;

public class FC1CalculatorTest {

	private FC1Calculator calc = new FC1Calculator();
	
	@Test
	public void noveltyNo() {
		FC1CardRating rating = FC1CardRating.builder().build();
		rating = (FC1CardRating) SerializationUtils.clone(rating);
		
		FC1CardRating judge = calc.judge(rating, "NO");
		assertThat(judge.getEasinessFactor()).isEqualTo(FC1Properties.EF_MIN);
		assertThat(judge.getNextRevision()).isEqualTo(LocalDate.now().plusDays(1l));
	}
	
	@Test
	public void noveltyYes() {
		FC1CardRating rating = FC1CardRating.builder().build();
		rating = (FC1CardRating) SerializationUtils.clone(rating);
		
		FC1CardRating judge = calc.judge(rating, "YES");
		assertThat(judge.getEasinessFactor()).isEqualTo(FC1Properties.EF_INIT);
		assertThat(judge.getNextRevision()).isEqualTo(LocalDate.now().plusDays(1l));
	}
	
	@Test
	public void remaindNo() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT)
				.lastRevision(LocalDate.now().minusDays(1))
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(11)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "NO");
		
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval()/2);
		assertThat(judge.getRepetition()).isEqualTo(1);
	}
	
	@Test
	public void remaindNoV2() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT)
				.lastRevision(LocalDate.now().minusDays(2))
				.firstRevision(LocalDate.now().minusDays(2l))
				.nextRevision(LocalDate.now().minusDays(1))
				.repetition(11)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "NO");
		
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval()/2);
		assertThat(judge.getRepetition()).isEqualTo(1);
	}
	
	@Test
	public void remaindYes() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT)
				.lastRevision(LocalDate.now().minusDays(1))
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(11)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "YES");
		
		assertThat(judge.getEasinessFactor()).isEqualTo(rating.getEasinessFactor() + FC1Properties.EF_SUCCESS_BONUS);
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval() * FC1Properties.INTERVAL_MULTIPLICATOR);
		assertThat(judge.getRepetition()).isEqualTo(1);
	}
	
	@Test
	public void remaindYesV2() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT)
				.lastRevision(LocalDate.now().minusDays(2))
				.firstRevision(LocalDate.now().minusDays(2))
				.nextRevision(LocalDate.now().minusDays(1))
				.repetition(11)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "YES");
		
		assertThat(judge.getEasinessFactor()).isEqualTo(rating.getEasinessFactor() + FC1Properties.EF_SUCCESS_BONUS);
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval() * FC1Properties.INTERVAL_MULTIPLICATOR);
		assertThat(judge.getRepetition()).isEqualTo(1);
	}
	
	@Test
	public void rescueYes() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT+0.1)
				.lastRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(1)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "YES");
		
		assertThat(judge.getEasinessFactor()).isEqualTo(rating.getEasinessFactor() + FC1Properties.EF_SUCCESS_BONUS);
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval());
		assertThat(judge.getRepetition()).isEqualTo(2);
	}
	
	@Test
	public void rescueNo() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT+0.1)
				.lastRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(1)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "NO");
		
		assertThat(judge.getEasinessFactor()).isEqualTo(rating.getEasinessFactor() - FC1Properties.EF_FAIL_BONUS);
		assertThat(judge.getInterval()).isEqualTo(FC1Properties.INTERVAL_INIT);
		assertThat(judge.getRepetition()).isEqualTo(2);
	}
	
	@Test
	public void learnNo() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT+0.1)
				.lastRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(3)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "NO");
		
		assertThat(judge.getEasinessFactor()).isEqualTo(rating.getEasinessFactor() - FC1Properties.EF_FAIL_BONUS);
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval());
		assertThat(judge.getRepetition()).isEqualTo(4);
	}
	
	@Test
	public void learnNo2Times() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT+0.1)
				.lastRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(3)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "NO");
		judge = calc.judge((FC1CardRating)SerializationUtils.clone(judge), "NO");
		
		assertThat(judge.getEasinessFactor())
			.isEqualTo(rating.getEasinessFactor() - FC1Properties.EF_FAIL_BONUS - FC1Properties.EF_FAIL_BONUS);
		assertThat(judge.getInterval()).isEqualTo(rating.getInterval());
		assertThat(judge.getRepetition()).isEqualTo(5);
	}
	
	@Test
	public void learnYes() {
		FC1CardRating rating = FC1CardRating.builder()
				.easinessFactor(FC1Properties.EF_INIT+0.1)
				.lastRevision(LocalDate.now())
				.firstRevision(LocalDate.now().minusDays(1l))
				.nextRevision(LocalDate.now())
				.repetition(3)
				.interval(10)
				.build();

		FC1CardRating judge = calc.judge((FC1CardRating)SerializationUtils.clone(rating), "YES");
		
		assertThat(judge.getEasinessFactor()).isEqualTo(rating.getEasinessFactor() + FC1Properties.EF_SUCCESS_BONUS);
		assertThat(judge.getInterval()).isEqualTo(FC1Properties.INTERVAL_INIT);
		assertThat(judge.getRepetition()).isEqualTo(4);
	}
}
