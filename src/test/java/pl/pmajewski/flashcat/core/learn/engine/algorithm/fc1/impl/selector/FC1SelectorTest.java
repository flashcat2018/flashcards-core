package pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.impl.selector;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.exception.FC1AlgorithmException;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository.FC1CardRatingRepository;
import pl.pmajewski.flashcat.core.learn.engine.algorithm.fc1.repository.FC1SessionRepository;

@RunWith(MockitoJUnitRunner.class)
public class FC1SelectorTest {

	private FC1Selector selector;
	@Mock
	private FC1CardRatingRepository repoMock;
	@Mock
	private FC1SessionRepository sessionRepo;
	
	@Before
	public void init() {
		selector = new FC1Selector(repoMock, sessionRepo);
	}
	
	@Test
	public void novelty() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.novelties())
			.thenReturn(FC1SelectorTestData.novelties())
			.thenReturn(FC1SelectorTestData.novelties())
			.thenReturn(FC1SelectorTestData.novelties());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isGreaterThanOrEqualTo(1l);
		assertThat(next.get()).isLessThanOrEqualTo(5l);
	}
	
	@Test
	public void remaind() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.remaind())
			.thenReturn(FC1SelectorTestData.remaind())
			.thenReturn(FC1SelectorTestData.remaind())
			.thenReturn(FC1SelectorTestData.remaind());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isGreaterThanOrEqualTo(6l);
		assertThat(next.get()).isLessThanOrEqualTo(9l);
	}
	
	@Test
	public void rescue() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.rescue())
			.thenReturn(FC1SelectorTestData.rescue())
			.thenReturn(FC1SelectorTestData.rescue())
			.thenReturn(FC1SelectorTestData.rescue());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isGreaterThanOrEqualTo(10l);
		assertThat(next.get()).isLessThanOrEqualTo(53l);
	}
	
	@Test
	public void learn() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.learn())
			.thenReturn(FC1SelectorTestData.learn())
			.thenReturn(FC1SelectorTestData.learn())
			.thenReturn(FC1SelectorTestData.learn());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isGreaterThanOrEqualTo(14l);
		assertThat(next.get()).isLessThanOrEqualTo(17l);
	}
	
	@Test
	public void progressiveRemain() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.progressiveRemaind())
			.thenReturn(FC1SelectorTestData.progressiveRemaind())
			.thenReturn(FC1SelectorTestData.progressiveRemaind())
			.thenReturn(FC1SelectorTestData.progressiveRemaind());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isEqualTo(20l);
	}
	
	@Test
	public void progressiveRescue() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.progressiveRescue())
			.thenReturn(FC1SelectorTestData.progressiveRescue())
			.thenReturn(FC1SelectorTestData.progressiveRescue())
			.thenReturn(FC1SelectorTestData.progressiveRescue());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isEqualTo(23l);
	}
	
	@Test
	public void progressiveLearn() throws FC1AlgorithmException {
		when(repoMock.streamByTally(any()))
			.thenReturn(FC1SelectorTestData.progressiveLearn())
			.thenReturn(FC1SelectorTestData.progressiveLearn())
			.thenReturn(FC1SelectorTestData.progressiveLearn())
			.thenReturn(FC1SelectorTestData.progressiveLearn());
		
		Optional<Long> next = selector.selectNextCard(1l);
		assertThat(next).isNotEmpty();
		assertThat(next.get()).isEqualTo(29l);
	}
}
