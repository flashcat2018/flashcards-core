package pl.pmajewski.flashcat.core.tally.service.impl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pmajewski.flashcat.core.tally.dto.TallyStatsDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
public class TallyServiceImplTest {

	@Autowired
	private TallyServiceImpl tallyService;

	@Test
	public void listForUser() {
		List<TallyStatsDTO> tallies = tallyService.list(1l);
		tallies.stream()
			.forEach(i -> System.out.println(i.getName()+" "+i.getNumberOfCardsToRepeat()+" "+i.getNumberOfNovelties()+" "+i.getNumberOfCards()));
	}
}
