package pl.pmajewski.flashcat.core.tally.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
public class TallyExportServiceImplTest {
	
	@Autowired
	private TallyExportServiceImpl exporterService;
	
	@Test
	public void test() throws IOException {
		ByteArrayResource resource = exporterService.generateXlsx(1l);
		File tempFile = File.createTempFile("tally", ".xlsx");
		System.out.println("TempFile - AbsolutePath="+tempFile.getAbsolutePath());
		try(BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(tempFile))) {
			os.write(resource.getByteArray());
			os.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
