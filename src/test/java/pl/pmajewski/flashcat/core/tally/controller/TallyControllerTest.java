package pl.pmajewski.flashcat.core.tally.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
public class TallyControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@Test
	public void talliesSuccess() throws Exception {
		MvcResult result = mvc.perform(get("/accounts/1/tallies").header("userId", 1))
			.andExpect(status().is2xxSuccessful())
			.andReturn();
		System.out.println(result.getResponse().getContentAsString());;
	}
}
