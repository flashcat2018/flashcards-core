package pl.pmajewski.flashcat.core.card.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class SearchUtilsTest {

	@Test
	public void test1() {
		assertThat(SearchUtils.toPage(9, 10)).isEqualTo(0);
	}
	
	@Test
	public void test2() {
		assertThat(SearchUtils.toPage(10, 0)).isEqualTo(0);
	}
	
	@Test
	public void test3() {
		assertThat(SearchUtils.toPage(25, 10)).isEqualTo(2);
	}
}
