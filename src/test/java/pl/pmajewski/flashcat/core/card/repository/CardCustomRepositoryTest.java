package pl.pmajewski.flashcat.core.card.repository;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.core.card.model.Card;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
public class CardCustomRepositoryTest {

	@Autowired
	private CardRepository cardRepo;
	
	@Test
	@Transactional
	public void findCardById() {
		Optional<Card> card = cardRepo.findById(625l);
		System.out.println("CardCustomRepositoryTest -> test1");
	}
}
