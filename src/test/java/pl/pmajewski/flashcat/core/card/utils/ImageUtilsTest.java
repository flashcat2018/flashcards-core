package pl.pmajewski.flashcat.core.card.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ImageUtilsTest {

	@Test
	public void jpeg() {
		String imgFormat = ImageUtils.mimeToImageFormat("image/jpeg");
		assertThat(imgFormat).isEqualTo("jpeg");
	}
}
