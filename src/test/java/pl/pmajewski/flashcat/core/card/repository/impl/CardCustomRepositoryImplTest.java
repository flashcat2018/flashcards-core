package pl.pmajewski.flashcat.core.card.repository.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.card.model.Card;
import pl.pmajewski.flashcat.core.card.repository.CardRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
@CommonsLog
@Transactional
public class CardCustomRepositoryImplTest {

	private static final Long tallyId = 2l;
	
	@Autowired
	private CardRepository cardRepo;
	
	@Test
	public void searchEmpty() {
		List<Card> result = cardRepo.searchQuestions(tallyId, List.of(), PageRequest.of(0, 100));
		log.info("SearchEmpty -> result.size="+result.size());
	}
}
