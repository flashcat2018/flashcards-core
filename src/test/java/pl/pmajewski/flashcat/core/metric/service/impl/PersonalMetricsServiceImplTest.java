package pl.pmajewski.flashcat.core.metric.service.impl;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.core.card.pojo.CardsMetricDTO;
import pl.pmajewski.flashcat.core.metric.dto.MetricsContainerDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
@CommonsLog
public class PersonalMetricsServiceImplTest {

	@Autowired
	private PersonalMetricsServiceImpl service;
	
	@Test
	public void test() {
		for(int i=0; i < 5; i++) {
			MetricsContainerDTO<CardsMetricDTO> repetitions = service.repetitions(1l, LocalDate.of(2019, 10, 01), LocalDate.now());
			System.out.println(repetitions);
		}
	}
}
