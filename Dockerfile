FROM adoptopenjdk/openjdk11

ADD ./target/core-1.0.jar /app/app.jar
CMD java -jar /app/app.jar 

EXPOSE 8080